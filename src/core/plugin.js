'use strict';
{
    const PLUGIN_ID = 'Eponesh_GameScore';
    const PLUGIN_VERSION = '1.0.33';
    const PLUGIN_CATEGORY = 'platform-specific';

    const PLUGIN_CLASS = (SDK.Plugins.Eponesh_GameScore = class GameScorePlugin extends SDK.IPluginBase {
        constructor() {
            super(PLUGIN_ID);

            SDK.Lang.PushContext('plugins.' + PLUGIN_ID.toLowerCase());

            this._info.SetName(lang('.name'));
            this._info.SetDescription(lang('.description'));
            this._info.SetVersion(PLUGIN_VERSION);
            this._info.SetCategory(PLUGIN_CATEGORY);
            this._info.SetAuthor('GamePush, FZCO');
            this._info.SetHelpUrl(lang('.help-url'));
            this._info.SetIsSingleGlobal(true);

            this._info.SetSupportedRuntimes(['c3']);

            SDK.Lang.PushContext('.properties');

            this._info.SetProperties([
                new SDK.PluginProperty('integer', 'project-id', 0),
                new SDK.PluginProperty('text', 'token', ''),
                new SDK.PluginProperty('check', 'show-preloader', false),
                new SDK.PluginProperty('check', 'should-wait-player', true),
                new SDK.PluginProperty('check', 'enabled', true),
                new SDK.PluginProperty('check', 'autoSendGameStart', true),
            ]);

            SDK.Lang.PopContext(); //.properties
            SDK.Lang.PopContext();
        }
    });

    PLUGIN_CLASS.Register(PLUGIN_ID, PLUGIN_CLASS);
}

'use strict';

{
    C3.Plugins.Eponesh_GameScore.Exps = {
        PlayerID() {
            return this.gp.player.id;
        },

        PlayerScore() {
            return this.gp.player.score;
        },

        PlayerName() {
            return this.gp.player.name;
        },

        PlayerAvatar() {
            return this.gp.player.avatar;
        },

        PlayerGet(key) {
            return this.gp.player.get(key);
        },

        PlayerHas(key) {
            return this.gp.player.is(key);
        },

        PlayerFieldName(key) {
            return this.gp.player.getFieldName(key);
        },

        PlayerFieldVariantName(key, value) {
            return this.gp.player.getFieldVariantName(key, value);
        },

        PlayerGetFieldVariantAt(key, index) {
            const variant = this.gp.player.getField(key).variants[index];
            return variant ? variant.value : '';
        },

        PlayerGetFieldVariantIndex(key, value) {
            return this.gp.player.getField(key).variants.findIndex((v) => v.value === value);
        },

        PlayerCurFieldKey() {
            return this.currentPlayerFieldKey || '';
        },

        PlayerCurFieldType() {
            return this.currentPlayerFieldType || '';
        },

        PlayerCurFieldName() {
            return this.currentPlayerFieldName || '';
        },

        PlayerCurFieldValue() {
            return typeof this.currentPlayerFieldValue === 'string'
                ? this.currentPlayerFieldValue
                : Number(this.currentPlayerFieldValue || 0);
        },

        PlayerCurFieldVariantValue() {
            return typeof this.currentPlayerFieldVariantValue === 'string'
                ? this.currentPlayerFieldVariantValue
                : Number(this.currentPlayerFieldVariantValue || 0);
        },

        PlayerCurFieldVariantName() {
            return this.currentPlayerFieldVariantName || '';
        },

        PlayerCurFieldVariantIndex() {
            return this.currentPlayerFieldVariantIndex || 0;
        },

        PlayerStatsPlaytimeAll() {
            return this.gp.player.stats.playtimeAll || 0;
        },
        PlayerStatsPlaytimeToday() {
            return this.gp.player.stats.playtimeToday || 0;
        },
        PlayerStatsActiveDays() {
            return this.gp.player.stats.activeDays || 0;
        },
        PlayerStatsActiveDaysConsecutive() {
            return this.gp.player.stats.activeDaysConsecutive || 0;
        },

        LeaderboardCurPlayerName() {
            return this.currentLeaderboardPlayer.name || '';
        },

        LeaderboardCurPlayerAvatar() {
            return this.currentLeaderboardPlayer.avatar || '';
        },

        LeaderboardCurPlayerID() {
            return this.currentLeaderboardPlayer.id || 0;
        },

        LeaderboardCurPlayerScore() {
            return this.currentLeaderboardPlayer.score || 0;
        },

        LeaderboardCurPlayerPosition() {
            return this.currentLeaderboardPlayer.position || 0;
        },

        LeaderboardCurPlayerIndex() {
            return this.currentLeaderboardIndex || 0;
        },

        LeaderboardCurPlayerField(key) {
            return key in this.currentLeaderboardPlayer ? this.currentLeaderboardPlayer[key] : 0;
        },

        LeaderboardPlayerFieldAt(index, key) {
            const player = this.leaderboard[index];
            return player && key in player ? player[key] : 0;
        },

        LeaderboardPlayerPosition() {
            return this.leaderboardPlayerPosition || 0;
        },

        LastLeaderboardTag() {
            return this.lastLeaderboardTag;
        },

        LastLeaderboardVariant() {
            return this.lastLeaderboardVariant;
        },

        IsFullscreenMode() {
            return Number(this.gp.fullscreen.isEnabled);
        },

        Language() {
            return this.gp.language;
        },

        AvatarGenerator() {
            return this.gp.avatarGenerator;
        },

        ServerTime() {
            return this.gp.serverTime;
        },

        ServerTimeUnix() {
            return new Date(this.gp.serverTime).getTime();
        },

        IsPaused() {
            return this.gp.isPaused;
        },

        IsGameplay() {
            return this.gp.isGameplay;
        },

        DeviceType() {
            return this.gp.device.type;
        },

        PlatformType() {
            return this.gp.platform.type;
        },

        AppTitle() {
            return this.gp.app.title;
        },

        AppDescription() {
            return this.gp.app.description;
        },

        AppImage() {
            return this.gp.app.image;
        },

        AppUrl() {
            return this.gp.app.url;
        },

        AppLastReviewRating() {
            return Number(this.appLastReviewRating) || 0;
        },

        AchievementsTotalAchievements() {
            return this.gp.achievements.list.length;
        },

        AchievementsTotalAchievementsGroups() {
            return this.gp.achievements.groupsList.length;
        },

        AchievementsTotalPlayerAchievements() {
            return this.gp.achievements.unlockedList.length;
        },

        AchievementsCurAchievementIndex() {
            return this.currentAchievementIndex;
        },

        AchievementsCurAchievementID() {
            return this.currentAchievement.id;
        },

        AchievementsCurAchievementTag() {
            return this.currentAchievement.tag;
        },

        AchievementsCurAchievementName() {
            return this.currentAchievement.name;
        },

        AchievementsCurAchievementDescription() {
            return this.currentAchievement.description;
        },

        AchievementsCurAchievementIcon() {
            return this.currentAchievement.icon;
        },

        AchievementsCurAchievementIconSmall() {
            return this.currentAchievement.iconSmall;
        },

        AchievementsCurAchievementRare() {
            return this.currentAchievement.rare;
        },

        AchievementsCurAchievementUnlocked() {
            return this.currentAchievement.unlocked;
        },

        AchievementsCurAchievementLockedIcon() {
            return this.currentAchievement.lockedIcon;
        },

        AchievementsCurAchievementLockedIconSmall() {
            return this.currentAchievement.lockedIconSmall;
        },

        AchievementsCurAchievementProgress() {
            return this.currentAchievement.progress;
        },

        AchievementsCurAchievementMaxProgress() {
            return this.currentAchievement.maxProgress;
        },

        AchievementsCurAchievementProgressStep() {
            return this.currentAchievement.progressStep;
        },

        AchievementsCurAchievementsGroupIndex() {
            return this.currentAchievementsGroupIndex;
        },

        AchievementsCurAchievementsGroupID() {
            return this.currentAchievementsGroupID;
        },

        AchievementsCurAchievementsGroupTag() {
            return this.currentAchievementsGroupTag;
        },

        AchievementsCurAchievementsGroupName() {
            return this.currentAchievementsGroupName;
        },

        AchievementsCurAchievementsGroupDescription() {
            return this.currentAchievementsGroupDescription;
        },

        AchievementsCurPlayerAchievementIndex() {
            return this.currentPlayerAchievementIndex;
        },

        AchievementsCurPlayerAchievementID() {
            return this.currentPlayerAchievementId;
        },

        AchievementsCurPlayerAchievementUnlockDate() {
            return this.currentPlayerAchievementUnlockDate;
        },

        AchievementsUnlockedAchievementSuccess() {
            return this.isUnlockAchievementSuccess;
        },

        AchievementsUnlockedAchievementError() {
            return this.unlockAchievementError;
        },

        AchievementsUnlockedAchievementID() {
            return this.currentAchievement.id;
        },

        AchievementsUnlockedAchievementTag() {
            return this.currentAchievement.tag;
        },

        AchievementsUnlockedAchievementName() {
            return this.currentAchievement.name;
        },

        AchievementsUnlockedAchievementDescription() {
            return this.currentAchievement.description;
        },

        AchievementsUnlockedAchievementIcon() {
            return this.currentAchievement.icon;
        },

        AchievementsUnlockedAchievementIconSmall() {
            return this.currentAchievement.iconSmall;
        },

        AchievementsUnlockedAchievementRare() {
            return this.currentAchievement.rare;
        },

        AchievementsGetProgress(idOrTag) {
            return this.gp.achievements.getProgress(idOrTag);
        },

        // socials
        SocialsCommunityLink() {
            return this.gp.socials.communityLink;
        },

        SocialsGetShareParam(key) {
            return this.gp.socials.getShareParam(key);
        },

        SocialsShareLink() {
            return this.gp.socials.makeShareUrl(this.shareParams);
        },

        // payments
        PaymentsCurProductIndex() {
            return this.currentProductIndex;
        },

        PaymentsCurProductID() {
            return this.currentProduct.id;
        },

        PaymentsCurProductTag() {
            return this.currentProduct.tag;
        },

        PaymentsCurProductName() {
            return this.currentProduct.name;
        },

        PaymentsCurProductDescription() {
            return this.currentProduct.description;
        },

        PaymentsCurProductIcon() {
            return this.currentProduct.icon;
        },

        PaymentsCurProductIconSmall() {
            return this.currentProduct.iconSmall;
        },

        PaymentsCurProductPrice() {
            return this.currentProduct.price;
        },

        PaymentsCurProductCurrency() {
            return this.currentProduct.currency;
        },

        PaymentsCurProductCurrencySymbol() {
            return this.currentProduct.currencySymbol;
        },

        PaymentsCurProductPurchases() {
            return this.currentProductPurchases;
        },

        PaymentsCurPurchaseIndex() {
            return this.currentPurchaseIndex;
        },

        PaymentsCurPurchaseID() {
            return this.currentPurchase.id || 0;
        },

        PaymentsCurPurchaseTag() {
            return this.currentPurchase.tag || '';
        },

        PaymentsCurPurchaseDate() {
            return this.currentPurchase.createdAt || '';
        },

        PaymentsCurPurchaseExpirationDate() {
            return this.currentPurchase.expiredAt || '';
        },

        PaymentsCurPurchaseSubscribed() {
            return Number(this.currentPurchase.subscribed || false) || 0;
        },

        PaymentsPurchasedProductSuccess() {
            return this.isPurchaseProductSuccess;
        },

        PaymentsPurchasedProductError() {
            return this.purchaseProductError;
        },

        PaymentsPurchasedProductID() {
            return this.purchasedProductId;
        },

        PaymentsPurchasedProductTag() {
            return this.purchasedProductTag;
        },

        PaymentsConsumedProductSuccess() {
            return this.isConsumeProductSuccess;
        },

        PaymentsConsumedProductError() {
            return this.consumeProductError;
        },

        PaymentsConsumedProductID() {
            return this.consumedProductId;
        },

        PaymentsConsumedProductTag() {
            return this.consumedProductTag;
        },

        PaymentsPurchaseDate(idOrTag) {
            const purchase = this.gp.payments.getPurchase(idOrTag);
            return purchase ? purchase.createdAt || '' : '';
        },

        PaymentsExpirationDate(idOrTag) {
            const purchase = this.gp.payments.getPurchase(idOrTag);
            return purchase ? purchase.expiredAt || '' : '';
        },

        // images
        ImagesCurImageIndex() {
            return this.currentImageIndex;
        },

        ImagesCurImageID() {
            return this.currentImage.id || '';
        },

        ImagesCurImageSrc() {
            return this.currentImage.src || '';
        },

        ImagesCurImageWidth() {
            return this.currentImage.width || 0;
        },

        ImagesCurImageHeight() {
            return this.currentImage.height || 0;
        },

        ImagesCurImagePlayerID() {
            return this.currentImage.playerId || 0;
        },

        ImagesCurImageTagIndex() {
            return this.currentImageTagIndex;
        },

        ImagesCurImageTag() {
            return this.currentImageTag || '';
        },

        ImagesTempFileUrl() {
            return this.lastImageTempUrl || '';
        },

        ImagesResize(url, width, height, crop) {
            return this.gp.images.resize(url, width, height, crop);
        },

        // files
        FilesCurFileIndex() {
            return this.currentFileIndex;
        },

        FilesCurFileID() {
            return this.currentFile.id || '';
        },

        FilesCurFileSrc() {
            return this.currentFile.src || '';
        },

        FilesCurFileName() {
            return this.currentFile.name || '';
        },

        FilesCurFileSize() {
            return this.currentFile.size || 0;
        },

        FilesCurFilePlayerID() {
            return this.currentFile.playerId || 0;
        },

        FilesCurFileTagIndex() {
            return this.currentFileTagIndex;
        },

        FilesCurFileTag() {
            return this.currentFileTag || '';
        },

        FilesTempFileUrl() {
            return this.lastFileTempUrl || '';
        },

        FilesLastFileContent() {
            return this.lastFileContent || '';
        },

        // games collections
        GamesCollectionsCollectionID() {
            return this.gamesCollection.id;
        },

        GamesCollectionsCollectionTag() {
            return this.gamesCollection.tag;
        },

        GamesCollectionsCollectionName() {
            return this.gamesCollection.name;
        },

        GamesCollectionsCollectionDescription() {
            return this.gamesCollection.description;
        },

        GamesCollectionsCurGameIndex() {
            return this.currentGameIndex;
        },

        GamesCollectionsCurGameID() {
            return this.currentGameId;
        },

        GamesCollectionsCurGameTag() {
            return this.currentGameTag;
        },

        GamesCollectionsCurGameName() {
            return this.currentGameName;
        },

        GamesCollectionsCurGameDescription() {
            return this.currentGameDescription;
        },

        GamesCollectionsCurGameIcon() {
            return this.currentGameIcon;
        },

        GamesCollectionsCurGameUrl() {
            return this.currentGameUrl;
        },

        GamesCollectionsFetchError() {
            return this.gamesCollectionFetchError;
        },

        // documents
        DocumentsDocumentType() {
            return this.document.type;
        },

        DocumentsDocumentContent() {
            return this.document.content;
        },

        DocumentsFetchError() {
            return this.documentFetchError;
        },

        // game variables
        VariablesGet(key) {
            return this.gp.variables.get(key);
        },

        VariablesHas(key) {
            return Number(this.gp.variables.has(key));
        },

        VariablesType(key) {
            return this.gp.variables.type(key);
        },

        VariablesCurIndex() {
            return this.currentVariableIndex || 0;
        },

        VariablesCurKey() {
            return this.currentVariable.key || '';
        },

        VariablesCurType() {
            return this.currentVariable.type || '';
        },

        VariablesCurValue() {
            return this.currentVariable.value || '';
        },

        // Players
        PlayersCurPlayerName() {
            return this.currentPlayersPlayer.state.name || '';
        },

        PlayersCurPlayerAvatar() {
            return this.currentPlayersPlayer.state.avatar || '';
        },

        PlayersCurPlayerID() {
            return this.currentPlayersPlayer.state.id || 0;
        },

        PlayersCurPlayerScore() {
            return this.currentPlayersPlayer.state.score || 0;
        },

        PlayersCurPlayerIndex() {
            return this.currentPlayersIndex || 0;
        },

        PlayersCurPlayerField(key) {
            return this.currentPlayersPlayer && key in this.currentPlayersPlayer.state
                ? this.currentPlayersPlayer.state[key]
                : 0;
        },

        PlayersPlayerFieldAt(index, key) {
            const player = this.playersList[index];
            return player && key in player.state ? player.state[key] : 0;
        },

        LastPlayersTag() {
            return this.lastPlayersTag;
        },

        RewardsCurIndex() {
            return this.curRewardIndex || 0;
        },
        RewardsCurID() {
            return this.curReward.id || 0;
        },
        RewardsCurTag() {
            return this.curReward.tag || '';
        },
        RewardsCurName() {
            return this.curReward.name || '';
        },
        RewardsCurDescription() {
            return this.curReward.description || '';
        },
        RewardsCurIcon() {
            return this.curReward.icon || '';
        },
        RewardsCurIconSmall() {
            return this.curReward.iconSmall || '';
        },
        RewardsCurCountTotal() {
            return this.curPlayerReward.countTotal || 0;
        },
        RewardsCurCountAccepted() {
            return this.curPlayerReward.countAccepted || 0;
        },

        TriggersCurIndex() {
            return this.curTriggerIndex || 0;
        },
        TriggersCurID() {
            return this.curTriggerInfo.trigger.id || 0;
        },
        TriggersCurTag() {
            return this.curTriggerInfo.trigger.tag || '';
        },
        TriggersCurDescription() {
            return this.curTriggerInfo.trigger.description || '';
        },

        BonusCurIndex() {
            return this.curBonusIndex || 0;
        },
        BonusCurID() {
            return this.curBonus.id || 0;
        },
        BonusCurType() {
            return this.curBonus.type || '';
        },

        SchedulersCurIndex() {
            return this.curSchedulerIndex || 0;
        },
        SchedulersCurID() {
            return this.curSchedulerInfo.scheduler.id || 0;
        },
        SchedulersCurTag() {
            return this.curSchedulerInfo.scheduler.tag || 0;
        },
        SchedulersCurType() {
            return this.curSchedulerInfo.scheduler.type || 0;
        },
        SchedulersCurDays() {
            return this.curSchedulerInfo.scheduler.days || 0;
        },
        SchedulersCurTodayDay() {
            return this.curSchedulerInfo.currentDay || 0;
        },
        SchedulersCurPlayerStatsActiveDays() {
            return (this.curSchedulerInfo.stats || {}).activeDays || 0;
        },
        SchedulersCurPlayerStatsActiveDaysConsecutive() {
            return (this.curSchedulerInfo.stats || {}).activeDaysConsecutive || 0;
        },
        SchedulersCurDayDayNumber() {
            return this.curSchedulerDayInfo.day || 0;
        },

        EventsCurIndex() {
            return this.curEventIndex || 0;
        },
        EventsCurID() {
            return this.curEventInfo.event.id || 0;
        },
        EventsCurTag() {
            return this.curEventInfo.event.tag || 0;
        },
        EventsCurName() {
            return this.curEventInfo.event.name || 0;
        },
        EventsCurDescription() {
            return this.curEventInfo.event.description || 0;
        },
        EventsCurIcon() {
            return this.curEventInfo.event.icon || 0;
        },
        EventsCurIconSmall() {
            return this.curEventInfo.event.iconSmall || 0;
        },
        EventsCurTimeLeft() {
            return this.curEventInfo.event.timeLeft || 0;
        },
        EventsCurPlayerStatsActiveDays() {
            return (this.curEventInfo.stats || {}).activeDays || 0;
        },
        EventsCurPlayerStatsActiveDaysConsecutive() {
            return (this.curEventInfo.stats || {}).activeDaysConsecutive || 0;
        },

        ExperimentsGet(tag) {
            return this.gp.experiments.map[tag] || '';
        },

        SegmentsCurSegment() {
            return this.curSegment || '';
        },

        LastError() {
            return this.lastError;
        },

        IsLastActionSuccess() {
            return Number(this.isLastActionSuccess);
        },

        AsJSON() {
            return JSON.stringify(this.SaveToJson());
        }
    };
}

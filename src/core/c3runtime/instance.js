'use strict'
{
  const SERVER_HOST = 'https://localhost:9000'
  // const SERVER_HOST = 'https://gs.eponesh.com/sdk';

  C3.Plugins.Eponesh_GameScore.Instance = class GameScoreInstance extends (
    C3.SDKInstanceBase
  ) {
    constructor(inst, properties) {
      super(inst)

      this.mappers = {
        language: [
          'en',
          'ru',
          'fr',
          'it',
          'de',
          'es',
          'zh',
          'pt',
          'ko',
          'ja',
          'tr',
          'ar',
          'hi',
          'id'
        ],
        avatarGenerator: [
          'dicebear_retro',
          'dicebear_identicon',
          'dicebear_human',
          'dicebear_micah',
          'dicebear_bottts',
          'icotar',
          'robohash_robots',
          'robohash_cats'
        ],
        order: ['default', 'DESC', 'ASC'],
        withMe: ['none', 'first', 'last'],
        platform: [
          'YANDEX',
          'VK',
          'NONE',
          'OK',
          'GAME_MONETIZE',
          'CRAZY_GAMES',
          'GAME_DISTRIBUTION',
          'SMARTMARKET',
          'GAMEPIX',
          'POKI',
          'VK_PLAY',
          'WG_PLAYGROUND',
          'KONGREGATE'
        ],
        deviceTypes: ['Desktop', 'IOS', 'Android', 'TV'],
        documentTypes: ['PLAYER_PRIVACY_POLICY'],
        documentFormat: ['HTML', 'TXT', 'RAW'],
        bonusType: ['REWARD', 'ACHIEVEMENT', 'PRODUCT'],
        schedulerType: ['ACTIVE_DAYS', 'ACTIVE_DAYS_CONSECUTIVE'],
        variablesTypes: ['stats', 'data', 'flag', 'image', 'file', 'doc_html'],
        compare: [
          (a, b) => a === b,
          (a, b) => a !== b,
          (a, b) => a < b,
          (a, b) => a <= b,
          (a, b) => a > b,
          (a, b) => a >= b
        ]
      }

      this.etos = function (err) {
        if (typeof err === 'string') {
          return err
        }
        return 'message' in err ? err.message : String(err)
      }

      this.getIdOrTag = (idOrTag) => {
        const id = parseInt(idOrTag, 10) || 0
        const query = id > 0 ? { id } : { tag: idOrTag }
        return query
      }

      this.conditions = C3.Plugins.Eponesh_GameScore.Cnds
      this.actions = C3.Plugins.Eponesh_GameScore.Acts
      this.handleResult = (success, err) => {
        this.isLastActionSuccess = !!success
        if (err) {
          this.lastError = this.etos(err)
          console.warn(err)
        }
      }

      this.awaiters = {
        player: {},
        gp: {}
      }
      this.awaiters.gp.ready = new Promise((res, rej) => {
        this.awaiters.gp.done = res
        this.awaiters.gp.abort = rej
      })
      this.awaiters.player.ready = new Promise((res, rej) => {
        this.awaiters.player.done = res
        this.awaiters.player.abort = rej
      })

      this.leaderboard = []
      this.leaderboardInfo = {}
      this.leaderboardRecords = {}
      this.leaderboardResult = {
        abovePlayers: [],
        belowPlayers: [],
        topPlayers: []
      }
      this.currentLeaderboardIndex = 0
      this.currentLeaderboardPlayer = {}
      this.lastLeaderboardTag = ''
      this.lastLeaderboardVariant = ''
      this.lastLeaderboardPlayerRatingTag = ''
      this.leaderboardPlayerPosition = 0

      this.currentPlayerFieldKey = ''
      this.currentPlayerFieldType = ''
      this.currentPlayerFieldName = ''
      this.currentPlayerFieldValue = ''

      this.currentPlayerFieldVariantValue = ''
      this.currentPlayerFieldVariantName = ''
      this.currentPlayerFieldVariantIndex = 0

      this.achievements = []
      this.achievementsGroups = []
      this.playerAchievements = []

      this.currentAchievementIndex = 0
      this.currentAchievement = {}

      this.currentAchievementsGroupIndex = 0
      this.currentAchievementsGroupId = 0
      this.currentAchievementsGroupTag = ''
      this.currentAchievementsGroupName = ''
      this.currentAchievementsGroupDescription = ''

      this.currentPlayerAchievementIndex = 0
      this.currentPlayerAchievementId = 0
      this.currentPlayerAchievementUnlockDate = ''

      this.isUnlockAchievementSuccess = false
      this.unlockAchievementError = ''

      this.isSetProgressAchievementSuccess = false
      this.setProgressAchievementError = ''

      this.products = []
      this.playerPurchases = []

      this.currentProductIndex = 0
      this.currentProduct = {
        id: 0,
        tag: '',
        name: '',
        description: '',
        icon: '',
        iconSmall: '',
        price: 0,
        currency: '',
        currencySymbol: ''
      }
      this.currentProductPurchases = 0

      this.currentPurchaseIndex = 0
      this.currentPurchase = {
        id: 0,
        tag: '',
        createdAt: '',
        expiredAt: '',
        subscribed: false
      }

      this.isSubscribeProductSuccess = false
      this.isUnsubscribeProductSuccess = false

      this.isPurchaseProductSuccess = false
      this.purchaseProductError = ''
      this.purchasedProductId = 0
      this.purchasedProductTag = ''

      this.isConsumeProductSuccess = false
      this.consumeProductError = ''
      this.consumedProductId = 0
      this.consumedProductTag = ''

      this.isLastAdSuccess = false
      this.isLastShareSuccess = false
      this.isLastCommunityJoinSuccess = false

      this.isLastAddShortcutSuccess = false

      this.isReady = false
      this.isPlayerReady = false

      this.gamesCollection = {
        id: 0,
        tag: '',
        name: '',
        description: '',
        games: []
      }

      this.currentGameIndex = 0
      this.currentGameId = 0
      this.currentGameName = ''
      this.currentGameDescription = ''
      this.currentGameIcon = ''
      this.currentGameUrl = ''
      this.gamesCollectionFetchError = ''
      this.lastGamesCollectionIdOrTag = ''

      this.document = {
        type: '',
        content: ''
      }

      this.lastDocumentType = ''
      this.documentFetchError = ''

      this.lastError = ''
      this.isLastActionSuccess = false

      this.shareLink = ''
      this.shareParams = {}

      this.images = []
      this.canLoadMoreImages = false
      this.currentImageIndex = 0
      this.currentImageTagIndex = 0
      this.currentImageTag = ''
      this.lastImageTempUrl = ''
      this.currentImage = {
        id: '',
        playerId: 0,
        width: 0,
        height: 0,
        src: '',
        tags: []
      }

      this.files = []
      this.lastFileContent = ''
      this.canLoadMoreFiles = false
      this.currentFileIndex = 0
      this.currentFileTagIndex = 0
      this.currentFileTag = ''
      this.lastFileTempUrl = ''
      this.currentFile = {
        id: '',
        playerId: 0,
        name: '',
        size: 0,
        src: '',
        tags: []
      }

      this.currentVariableIndex = 0
      this.currentVariable = {
        key: '',
        type: '',
        value: ''
      }

      this.currentPlayersIndex = 0
      this.lastPlayersTag = ''
      this.currentPlayersPlayer = {
        state: {},
        achievements: [],
        purchases: []
      }

      this.curRewardIndex = 0
      this.curReward = {}
      this.curPlayerReward = {}

      this.setReward = (result = {}, index = 0) => {
        this.curRewardIndex = index
        this.curReward = result.reward || {}
        this.curPlayerReward = result.playerReward || {}
      }

      this.curTriggerIndex = 0
      this.curTriggerInfo = { trigger: {} }

      this.setTriggerInfo = (idOrTag, index = 0) => {
        const result = this.gp.triggers.getTrigger(idOrTag)
        if (!result.trigger) {
          result.trigger = {}
        }
        this.curTriggerIndex = index
        this.curTriggerInfo = result || { trigger: {} }
      }

      this.curBonusIndex = 0
      this.curBonus = {}

      this.setBonus = (result, index = 0) => {
        this.curBonusIndex = index
        this.curBonus = result || {}
      }

      this.curSchedulerIndex = 0
      this.curSchedulerInfo = { scheduler: {} }
      this.curSchedulerDayInfo = { scheduler: {} }

      this.setSchedulerInfo = (idOrTag, index = 0) => {
        const result = this.gp.schedulers.getScheduler(idOrTag)
        if (!result.scheduler) {
          result.scheduler = {}
        }
        this.curSchedulerIndex = index
        this.curSchedulerInfo = result || { scheduler: {} }
        this.setSchedulerDayInfo(idOrTag, 1, index)
      }

      this.setSchedulerDayInfo = (idOrTag, day, index = 0) => {
        const result = this.gp.schedulers.getSchedulerDay(idOrTag, day)
        if (!result.scheduler) {
          result.scheduler = {}
        }
        this.curSchedulerDayInfo = result || { scheduler: {} }
      }

      this.curEventIndex = 0
      this.curEventInfo = { event: {} }

      this.setEventInfo = (idOrTag, index = 0) => {
        const result = this.gp.events.getEvent(idOrTag)
        if (!result.event) {
          result.event = {}
        }
        this.curEventIndex = index
        this.curEventInfo = result || { event: {} }
      }

      this.lastIdOrTag = { id: 0, tag: '' }
      this.lastPickedSchedulerDay = 0
      this.lastPickedSchedulerTriggerIdOrTag = ''
      this.isPickedIdOrTag = (idOrTag) => {
        const q = this.getIdOrTag(idOrTag)
        return (
          (q.id > 0 && q.id === this.lastIdOrTag.id) ||
          (q.tag != '' && q.tag === this.lastIdOrTag.tag)
        )
      }
      this.isPickedSchedulerDayAndTrigger = (idOrTag, day, triggerIdOrTag) => {
        const isPickedScheduler = !idOrTag || this.isPickedIdOrTag(idOrTag)
        const isPickedDay = !day || day === this.lastPickedSchedulerDay
        const isPickedTrigger =
          !triggerIdOrTag ||
          triggerIdOrTag === this.lastPickedSchedulerTriggerIdOrTag
        return isPickedScheduler && isPickedDay && isPickedTrigger
      }

      this.curSegment = ''

      this.projectId = Number(properties[0] || 0)
      this.publicToken = properties[1]
      this.showPreloaderOnStart = properties[2]
      this.shouldWaitPlayerOnLoad = properties[3]
      this.isEnabled = properties[4]
      this.isAutoSendGameStart = properties[5]

      this._runtime.AddLoadPromise(this.awaiters.gp.ready)
      if (this.shouldWaitPlayerOnLoad) {
        this._runtime.AddLoadPromise(this.awaiters.player.ready)
      }

      if (!this.isEnabled) {
        this.onError('[GamePush] disabled')
        this.awaiters.gp.done()
        this.awaiters.player.done()
        return
      }

      this._runtime
        .Dispatcher()
        .addEventListener('afterfirstlayoutstart', () => {
          const iRuntime = this._runtime.GetIRuntime()
          if (iRuntime) {
            iRuntime.GameScore = this.gp
            iRuntime.GamePush = this.gp
          }

          if (this.isReady) {
            this.Trigger(this.conditions.OnReady)
          }

          if (this.isPlayerReady) {
            this.Trigger(this.conditions.OnPlayerReady)
          }

          if (this.isAutoSendGameStart) {
            this.gp.gameStart()
          }
        })

      this.loadLib()
    }

    onError(err) {
      console.warn(err)
      const stub = () => Promise.resolve({})
      this.awaiters.gp.done()
      this.awaiters.player.done()
      this.gp = {
        on() {},
        changeLanguage: stub,
        changeAvatarGenerator: stub,
        loadOverlay: stub,
        pause: stub,
        resume: stub,
        gameStart: stub,
        gameplayStart: stub,
        gameplayStop: stub,
        isDev: false,
        isPaused: false,
        isGameplay: false,
        isPortrait: false,
        language: 'en',
        avatarGenerator: 'dicebear_retro',
        app: {
          on() {},
          title: '',
          description: '',
          image: '',
          url: '',
          canAddShortcut: false,
          addShortcut: stub
        },
        device: {
          on() {},
          type: ''
        },
        analytics: {
          on() {},
          hit() {},
          goal() {}
        },
        platform: {
          on() {},
          hasIntegratedAuth: false,
          type: 'NONE'
        },
        socials: {
          isSupportsNativeShare: false,
          isSupportsNativePosts: false,
          isSupportsNativeInvite: false,
          share: stub,
          post: stub,
          invite: stub,
          makeShareUrl: () => ''
        },
        leaderboard: {
          on() {},
          open: stub,
          fetch: stub,
          fetchScoped: stub,
          fetchPlayerRating: stub,
          fetchPlayerRatingScoped: stub
        },
        achievements: {
          on() {},
          has() {},
          open: stub,
          fetch: stub,
          unlock: stub,
          setProgress: stub
        },
        gamesCollections: {
          on() {},
          open: stub,
          fetch: stub
        },
        documents: {
          on() {},
          open: stub,
          fetch: stub
        },
        variables: {
          list: [],
          on() {},
          fetch: stub,
          get: stub,
          has: stub,
          type: stub
        },
        images: {
          canUpload: false,
          list: [],
          on() {},
          upload: stub,
          uploadUrl: stub,
          fetch: stub,
          fetchMore: stub,
          chooseFile: stub,
          resize: stub
        },
        files: {
          canUpload: false,
          list: [],
          on() {},
          upload: stub,
          uploadUrl: stub,
          uploadContent: stub,
          loadContent: stub,
          fetch: stub,
          fetchMore: stub,
          chooseFile: stub
        },
        payments: {
          isAvailable: false,
          purchases: [],
          on() {},
          has() {},
          fetchProducts: stub,
          purchase: stub,
          consume: stub
        },
        fullscreen: {
          isEnabled: false,
          on() {},
          open() {},
          close() {},
          toggle() {}
        },
        ads: {
          isFullscreenAvailable: false,
          isRewardedAvailable: false,
          isPreloaderAvailable: false,
          isStickyAvailable: false,
          isAdblockEnabled: false,
          on() {},
          showFullscreen: stub,
          showRewardedVideo: stub,
          showPreloader: stub,
          showSticky: stub,
          closeSticky: stub,
          refreshSticky: stub
        },
        player: {
          isStub: true,
          isLoggedIn: false,
          id: 0,
          name: '',
          avatar: '',
          stats: {},
          on() {},
          sync: stub,
          load: stub,
          login: stub,
          fetchFields: stub,
          getField: stub,
          getFieldName: stub,
          getFieldVariantName: stub,
          add: stub,
          has: stub,
          get: stub,
          set: stub,
          toggle: stub,
          reset: stub,
          remove: stub,
          toJSON: () => ({}),
          fields: []
        },
        players: {
          on() {},
          fetch: stub
        },
        rewards: {
          on() {},
          list: [],
          givenList: [],
          accept: stub,
          give: stub,
          has: stub,
          hasAccepted: stub,
          hasUnaccepted: stub,
          getReward: stub
        },
        triggers: {
          on() {},
          list: [],
          activatedList: [],
          claim: stub,
          isActivated: stub,
          isClaimed: stub,
          getTrigger: stub
        },
        schedulers: {
          on() {},
          list: [],
          activeList: [],
          getSchedulersTriggers: stub,
          getScheduler: stub,
          isRegistered: stub,
          isTodayRewardClaimed: stub,
          canClaimDay: stub,
          canClaimDayAdditional: stub,
          canClaimAllDay: stub,
          getSchedulerDay: stub,
          getSchedulerCurrentDay: stub,
          claimDay: stub,
          claimDayAdditional: stub,
          claimAllDay: stub,
          claimAllDays: stub
        },
        events: {
          on() {},
          list: [],
          activeList: [],
          getEvent: stub,
          has: stub,
          join: stub,
          isJoined: stub
        }
      }
      this.isReady = true
      this.Trigger(this.conditions.OnReady)
      this.isPlayerReady = true
      this.Trigger(this.conditions.OnPlayerReady)
    }

    loadLib() {
      try {
        window.onGPInit = (gp) =>
          gp.ready.then(() => this.init(gp)).catch((err) => this.onError(err))
        ;((d) => {
          var t = d.getElementsByTagName('script')[0]
          var s = d.createElement('script')
          s.src = `${SERVER_HOST}/gamepush.js?projectId=${this.projectId}&publicToken=${this.publicToken}&callback=onGPInit`
          s.async = true
          s.onerror = (err) => this.onError(err)
          t.parentNode.insertBefore(s, t)
        })(document)
      } catch (err) {
        console.error(err)
        this.onError(err)
      }
    }

    init(gp) {
      this.gp = gp

      const iRuntime = this._runtime.GetIRuntime()
      if (iRuntime) {
        iRuntime.GameScore = this.gp
        iRuntime.GamePush = this.gp
      }

      // player
      this.gp.player.on('ready', () => {
        this.isPlayerReady = true
        this.awaiters.player.done()
        this.Trigger(this.conditions.OnPlayerReady)
      })
      this.gp.player.on('change', () =>
        this.Trigger(this.conditions.OnPlayerChange)
      )
      this.gp.player.on('sync', (success) => {
        this.handleResult(success)
        this.Trigger(
          success
            ? this.conditions.OnPlayerSyncComplete
            : this.conditions.OnPlayerSyncError
        )
      })
      this.gp.player.on('load', (success) => {
        this.handleResult(success)
        this.Trigger(
          success
            ? this.conditions.OnPlayerLoadComplete
            : this.conditions.OnPlayerLoadError
        )
      })
      this.gp.player.on('login', (success) => {
        this.handleResult(success)
        this.Trigger(
          success
            ? this.conditions.OnPlayerLoginComplete
            : this.conditions.OnPlayerLoginError
        )
      })
      this.gp.player.on('fetchFields', (success) => {
        this.handleResult(success)
        this.Trigger(
          success
            ? this.conditions.OnPlayerFetchFieldsComplete
            : this.conditions.OnPlayerFetchFieldsError
        )
      })

      // leaderboard
      this.gp.leaderboard.on('open', () =>
        this.Trigger(this.conditions.OnLeaderboardOpen)
      )
      this.gp.leaderboard.on('close', () =>
        this.Trigger(this.conditions.OnLeaderboardClose)
      )

      // achievements
      this.gp.achievements.on('open', () =>
        this.Trigger(this.conditions.OnAchievementsOpen)
      )
      this.gp.achievements.on('close', () =>
        this.Trigger(this.conditions.OnAchievementsClose)
      )
      this.gp.achievements.on('unlock', (achievement) => {
        this.handleResult(true)
        this.isUnlockAchievementSuccess = true
        this.unlockAchievementError = ''
        this.currentAchievement = achievement || {}
        this.Trigger(this.conditions.OnAchievementsUnlock)
        this.Trigger(this.conditions.OnAchievementsAnyUnlock)
      })
      this.gp.achievements.on('error:unlock', (err, payload) => {
        this.lastIdOrTag = payload.input
        this.isUnlockAchievementSuccess = false
        this.unlockAchievementError = this.etos(err)
        this.handleResult(false, err)
        this.Trigger(this.conditions.OnAchievementsAnyUnlockError)
      })
      this.gp.achievements.on('progress', (achievement) => {
        this.handleResult(true)
        this.currentAchievement = achievement || {}
        this.Trigger(this.conditions.OnAchievementsSetProgress)
        this.Trigger(this.conditions.OnAchievementsAnySetProgress)
      })
      this.gp.achievements.on('error:progress', (err, payload) => {
        this.lastIdOrTag = payload.input
        this.handleResult(false, err)
        this.Trigger(this.conditions.OnAchievementsAnySetProgressError)
      })

      // payments
      this.gp.payments.on('purchase', (result) => {
        this.isPurchaseProductSuccess = true
        this.purchaseProductError = ''
        this.handleResult(true)

        const product = result.product || {}
        this.purchasedProductId = product.id || 0
        this.purchasedProductTag = product.tag || ''

        this.Trigger(this.conditions.OnPaymentsPurchase)
        this.Trigger(this.conditions.OnPaymentsAnyPurchase)
      })
      this.gp.payments.on('error:purchase', (err, payload) => {
        this.lastIdOrTag = payload.input
        this.isPurchaseProductSuccess = false
        this.purchasedProductId = payload.input.id || 0
        this.purchasedProductTag = payload.input.tag || ''
        this.purchaseProductError = this.etos(err)

        this.handleResult(false, err)
        this.Trigger(this.conditions.OnPaymentsPurchaseError)
        this.Trigger(this.conditions.OnPaymentsAnyPurchaseError)
      })

      this.gp.payments.on('subscribe', (result) => {
        this.isSubscribeProductSuccess = true
        this.handleResult(true)

        const product = result.product || {}
        const purchase = result.purchase || {}
        this.currentPurchase = purchase
        this.currentProduct = product
        this.currentProductPurchases = 1

        this.purchasedProductId = product.id || 0
        this.purchasedProductTag = product.tag || ''

        this.Trigger(this.conditions.OnPaymentsSubscribe)
        this.Trigger(this.conditions.OnPaymentsAnySubscribe)
      })
      this.gp.payments.on('error:subscribe', (err, payload) => {
        this.lastIdOrTag = payload.input
        this.isSubscribeProductSuccess = false
        this.purchasedProductId = payload.input.id || 0
        this.purchasedProductTag = payload.input.tag || ''

        this.isSubscribeProductSuccess = false
        this.handleResult(false, err)

        this.Trigger(this.conditions.OnPaymentsSubscribeError)
        this.Trigger(this.conditions.OnPaymentsAnySubscribeError)
      })

      // games collections
      this.gp.gamesCollections.on('open', () =>
        this.Trigger(this.conditions.OnGamesCollectionsOpen)
      )
      this.gp.gamesCollections.on('close', () =>
        this.Trigger(this.conditions.OnGamesCollectionsClose)
      )

      this.gp.documents.on('open', () =>
        this.Trigger(this.conditions.OnDocumentsOpen)
      )
      this.gp.documents.on('close', () =>
        this.Trigger(this.conditions.OnDocumentsClose)
      )

      // fullscreen
      this.gp.fullscreen.on('open', () =>
        this.Trigger(this.conditions.OnFullscreenOpen)
      )
      this.gp.fullscreen.on('close', () =>
        this.Trigger(this.conditions.OnFullscreenClose)
      )
      this.gp.fullscreen.on('change', () =>
        this.Trigger(this.conditions.OnFullscreenChange)
      )

      // ads
      this.gp.ads.on('start', () => this.Trigger(this.conditions.OnAdsStart))
      this.gp.ads.on('close', (success) => {
        this.handleResult(success)
        this.isLastAdSuccess = success
        this.Trigger(this.conditions.OnAdsClose)
      })

      this.gp.ads.on('fullscreen:start', () =>
        this.Trigger(this.conditions.OnAdsFullscreenStart)
      )
      this.gp.ads.on('fullscreen:close', () =>
        this.Trigger(this.conditions.OnAdsFullscreenClose)
      )

      this.gp.ads.on('preloader:start', () =>
        this.Trigger(this.conditions.OnAdsPreloaderStart)
      )
      this.gp.ads.on('preloader:close', () =>
        this.Trigger(this.conditions.OnAdsPreloaderClose)
      )

      this.gp.ads.on('rewarded:start', () =>
        this.Trigger(this.conditions.OnAdsRewardedStart)
      )
      this.gp.ads.on('rewarded:close', () =>
        this.Trigger(this.conditions.OnAdsRewardedClose)
      )
      this.gp.ads.on('rewarded:reward', () =>
        this.Trigger(this.conditions.OnAdsRewardedReward)
      )

      this.gp.ads.on('sticky:start', () =>
        this.Trigger(this.conditions.OnAdsStickyStart)
      )
      this.gp.ads.on('sticky:close', () =>
        this.Trigger(this.conditions.OnAdsStickyClose)
      )
      this.gp.ads.on('sticky:refresh', () =>
        this.Trigger(this.conditions.OnAdsStickyRefresh)
      )
      this.gp.ads.on('sticky:render', () =>
        this.Trigger(this.conditions.OnAdsStickyRender)
      )

      // socials
      this.gp.socials.on('share', (success) => {
        this.handleResult(success)
        this.isLastShareSuccess = success
        this.Trigger(this.conditions.OnSocialsShare)
      })
      this.gp.socials.on('post', (success) => {
        this.handleResult(success)
        this.isLastShareSuccess = success
        this.Trigger(this.conditions.OnSocialsPost)
      })
      this.gp.socials.on('invite', (success) => {
        this.handleResult(success)
        this.isLastShareSuccess = success
        this.Trigger(this.conditions.OnSocialsInvite)
      })
      this.gp.socials.on('joinCommunity', (success) => {
        this.handleResult(success)
        this.isLastCommunityJoinSuccess = success
        this.Trigger(this.conditions.OnSocialsJoinCommunity)
      })

      // app
      this.gp.app.on('addShortcut', (success) => {
        this.handleResult(success)
        this.isLastAddShortcutSuccess = success
        this.Trigger(this.conditions.OnAppAddShortcut)
      })

      this.gp.app.on('review', (result) => {
        this.handleResult(true)
        this.appLastReviewRating = result.rating || 0
        this.Trigger(this.conditions.OnAppReview)
      })

      this.gp.app.on('error:review', (err) => {
        this.handleResult(false, err)
        this.appLastReviewRating = 0
        this.Trigger(this.conditions.OnAppReviewError)
      })

      // gp
      this.gp.on('change:language', () =>
        this.Trigger(this.conditions.OnChangeLanguage)
      )
      this.gp.on('change:avatarGenerator', () =>
        this.Trigger(this.conditions.OnChangeAvatarGenerator)
      )
      this.gp.on('change:orientation', () =>
        this.Trigger(this.conditions.OnChangeOrientation)
      )
      this.gp.on('overlay:ready', () =>
        this.Trigger(this.conditions.OnOverlayReady)
      )

      this.gp.on('pause', () => this.Trigger(this.conditions.OnPause))
      this.gp.on('resume', () => this.Trigger(this.conditions.OnResume))

      this.gp.on('gameplayStart', () =>
        this.Trigger(this.conditions.OnGameplayStart)
      )
      this.gp.on('gameplayStop', () =>
        this.Trigger(this.conditions.OnGameplayStop)
      )

      this.gp.channels.on('event', (event) => {
        this.curEvent = event
      })

      this.gp.on('event:connect', () => {
        this.Trigger(this.conditions.OnEventConnect)
      })

      this.gp.rewards.on('accept', (result) => {
        this.setReward(result)
        this.handleResult(true)
        this.lastIdOrTag = { id: result.reward.id, tag: result.reward.tag }
        this.Trigger(this.conditions.OnRewardsAccept)
      })
      this.gp.rewards.on('error:accept', (err, payload) => {
        this.handleResult(false, err)
        this.lastIdOrTag = payload.input
        this.Trigger(this.conditions.OnRewardsAcceptError)
      })
      this.gp.rewards.on('give', (result) => {
        this.setReward(result)
        this.handleResult(true)
        this.lastIdOrTag = { id: result.reward.id, tag: result.reward.tag }
        this.Trigger(this.conditions.OnRewardsGive)
      })
      this.gp.rewards.on('error:give', (err, payload) => {
        this.handleResult(false, err)
        this.lastIdOrTag = payload.input
        this.Trigger(this.conditions.OnRewardsGiveError)
      })

      this.gp.triggers.on('activate', (result) => {
        this.setTriggerInfo(result.trigger.id)
        this.handleResult(true)
        this.lastIdOrTag = { id: result.trigger.id, tag: result.trigger.tag }
        this.Trigger(this.conditions.OnTriggersActivate)
      })
      this.gp.triggers.on('claim', (result) => {
        this.setTriggerInfo(result.trigger.id)
        this.handleResult(true)
        this.lastIdOrTag = { id: result.trigger.id, tag: result.trigger.tag }
        this.Trigger(this.conditions.OnTriggersClaim)
      })
      this.gp.triggers.on('error:claim', (err, payload) => {
        this.handleResult(false, err)
        this.lastIdOrTag = payload.input
        this.Trigger(this.conditions.OnTriggersClaimError)
      })

      this.gp.schedulers.on('register', (result) => {
        this.setSchedulerInfo(result.scheduler.id)
        this.handleResult(true)
        this.lastIdOrTag = {
          id: result.scheduler.id,
          tag: result.scheduler.tag
        }
        this.Trigger(this.conditions.OnSchedulersRegister)
      })
      this.gp.schedulers.on('error:register', (err, payload) => {
        this.handleResult(false, err)
        this.lastIdOrTag = payload.input
        this.Trigger(this.conditions.OnSchedulersRegisterError)
      })

      this.gp.schedulers.on('claimDay', (result) => {
        this.setSchedulerInfo(result.scheduler.id)
        this.setSchedulerDayInfo(result.scheduler.id, result.day)
        this.handleResult(true)
        this.lastIdOrTag = {
          id: result.scheduler.id,
          tag: result.scheduler.tag
        }
        this.lastPickedSchedulerDay = result.day
        this.Trigger(this.conditions.OnSchedulersClaimDay)
      })
      this.gp.schedulers.on('error:claimDay', (err, payload) => {
        this.handleResult(false, err)
        this.lastIdOrTag = payload.input
        this.lastPickedSchedulerDay = payload.input.day
        this.Trigger(this.conditions.OnSchedulersClaimDayError)
      })
      this.gp.schedulers.on('claimDayAdditional', (result, payload) => {
        this.setSchedulerInfo(result.scheduler.id)
        this.setSchedulerDayInfo(result.scheduler.id, result.day)
        this.setTriggerInfo(payload.input.triggerIdOrTag)
        this.handleResult(true)
        this.lastIdOrTag = {
          id: result.scheduler.id,
          tag: result.scheduler.tag
        }
        this.lastPickedSchedulerDay = result.day
        this.lastPickedSchedulerTriggerIdOrTag = payload.input.triggerIdOrTag
        this.Trigger(this.conditions.OnSchedulersClaimDayAdditional)
      })
      this.gp.schedulers.on('error:claimDayAdditional', (err, payload) => {
        this.handleResult(false, err)
        this.lastIdOrTag = payload.input
        this.lastPickedSchedulerDay = payload.input.day
        this.lastPickedSchedulerTriggerIdOrTag = payload.input.triggerIdOrTag
        this.Trigger(this.conditions.OnSchedulersClaimDayAdditionalError)
      })
      this.gp.schedulers.on('claimAllDay', (result) => {
        this.setSchedulerInfo(result.scheduler.id)
        this.setSchedulerDayInfo(result.scheduler.id, result.day)
        this.handleResult(true)
        this.lastIdOrTag = {
          id: result.scheduler.id,
          tag: result.scheduler.tag
        }
        this.lastPickedSchedulerDay = result.day
        this.Trigger(this.conditions.OnSchedulersClaimAllDay)
      })
      this.gp.schedulers.on('error:claimAllDay', (err, payload) => {
        this.handleResult(false, err)
        this.lastIdOrTag = payload.input
        this.lastPickedSchedulerDay = payload.input.day
        this.Trigger(this.conditions.OnSchedulersClaimAllDayError)
      })
      this.gp.schedulers.on('claimAllDays', (result) => {
        this.setSchedulerInfo(result.scheduler.id)
        this.handleResult(true)
        this.lastIdOrTag = {
          id: result.scheduler.id,
          tag: result.scheduler.tag
        }
        this.Trigger(this.conditions.OnSchedulersClaimAllDays)
      })
      this.gp.schedulers.on('error:claimAllDays', (err, payload) => {
        this.handleResult(false, err)
        this.lastIdOrTag = payload.input
        this.Trigger(this.conditions.OnSchedulersClaimAllDaysError)
      })

      this.gp.events.on('join', (result) => {
        this.setEventInfo(result.event.id)
        this.handleResult(true)
        this.lastIdOrTag = { id: result.event.id, tag: result.event.tag }
        this.Trigger(this.conditions.OnEventsJoin)
      })
      this.gp.events.on('error:join', (err, payload) => {
        this.handleResult(false, err)
        this.lastIdOrTag = payload.input
        this.Trigger(this.conditions.OnEventsJoinError)
      })

      this.gp.segments.on('enter', (segment) => {
        this.curSegment = segment
        this.Trigger(this.conditions.OnSegmentsEnter)
      })

      this.gp.segments.on('leave', (segment) => {
        this.curSegment = segment
        this.Trigger(this.conditions.OnSegmentsLeave)
      })

      // ready
      this.isReady = true
      this.Trigger(this.conditions.OnReady)
      this.awaiters.gp.done()

      if (this.showPreloaderOnStart) {
        this.gp.ads.showPreloader()
      }
    }

    Release() {
      super.Release()
    }

    SaveToJson() {
      return {
        leaderboard: this.leaderboard,
        leaderboardInfo: this.leaderboardInfo,
        leaderboardRecords: this.leaderboardRecords,
        leaderboardResult: this.leaderboardResult,

        currentLeaderboardIndex: this.currentLeaderboardIndex,
        currentLeaderboardPlayer: this.currentLeaderboardPlayer,
        lastLeaderboardTag: this.lastLeaderboardTag,
        lastLeaderboardVariant: this.lastLeaderboardVariant,
        lastLeaderboardPlayerRatingTag: this.lastLeaderboardPlayerRatingTag,
        leaderboardPlayerPosition: this.leaderboardPlayerPosition,

        currentPlayerFieldKey: this.currentPlayerFieldKey,
        currentPlayerFieldType: this.currentPlayerFieldType,
        currentPlayerFieldName: this.currentPlayerFieldName,
        currentPlayerFieldValue: this.currentPlayerFieldValue,

        currentPlayerFieldVariantValue: this.currentPlayerFieldVariantValue,
        currentPlayerFieldVariantName: this.currentPlayerFieldVariantName,
        currentPlayerFieldVariantIndex: this.currentPlayerFieldVariantIndex,

        isLastAdSuccess: this.isLastAdSuccess,
        isLastShareSuccess: this.isLastShareSuccess,
        isLastCommunityJoinSuccess: this.isLastCommunityJoinSuccess,

        isLastAddShortcutSuccess: this.isLastAddShortcutSuccess,

        isReady: this.isReady,
        isPlayerReady: this.isPlayerReady,

        achievements: this.achievements,
        achievementsGroups: this.achievementsGroups,
        playerAchievements: this.playerAchievements,

        currentAchievementIndex: this.currentAchievementIndex,
        currentAchievement: this.currentAchievement,

        currentAchievementsGroupIndex: this.currentAchievementsGroupIndex,
        currentAchievementsGroupId: this.currentAchievementsGroupId,
        currentAchievementsGroupTag: this.currentAchievementsGroupTag,
        currentAchievementsGroupName: this.currentAchievementsGroupName,
        currentAchievementsGroupDescription:
          this.currentAchievementsGroupDescription,

        currentPlayerAchievementIndex: this.currentPlayerAchievementIndex,
        currentPlayerAchievementId: this.currentPlayerAchievementId,
        currentPlayerAchievementUnlockDate:
          this.currentPlayerAchievementUnlockDate,

        isUnlockAchievementSuccess: this.isUnlockAchievementSuccess,
        unlockAchievementError: this.unlockAchievementError,

        products: this.products,
        playerPurchases: this.playerPurchases,

        currentProductIndex: this.currentProductIndex,
        currentProduct: this.currentProduct,
        currentProductPurchases: this.currentProductPurchases,

        currentPurchaseIndex: this.currentPurchaseIndex,
        currentPurchase: this.currentPurchase,

        lastPlayersTag: this.lastPlayersTag,
        currentPlayersIndex: this.currentPlayersIndex,
        currentPlayersPlayer: this.currentPlayersPlayer,

        isSubscribeProductSuccess: this.isSubscribeProductSuccess,
        isUnsubscribeProductSuccess: this.isUnsubscribeProductSuccess,

        isPurchaseProductSuccess: this.isPurchaseProductSuccess,
        purchaseProductError: this.purchaseProductError,
        purchasedProductId: this.purchasedProductId,
        purchasedProductTag: this.purchasedProductTag,

        isConsumeProductSuccess: this.isConsumeProductSuccess,
        consumeProductError: this.consumeProductError,
        consumedProductId: this.consumedProductId,
        consumedProductTag: this.consumedProductTag,

        gamesCollection: this.gamesCollection,

        currentGameIndex: this.currentGameIndex,
        currentGameId: this.currentGameId,
        currentGameName: this.currentGameName,
        currentGameDescription: this.currentGameDescription,
        currentGameIcon: this.currentGameIcon,
        currentGameUrl: this.currentGameUrl,

        gamesCollectionFetchError: this.gamesCollectionFetchError,
        lastGamesCollectionIdOrTag: this.lastGamesCollectionIdOrTag,

        document: this.document,
        lastDocumentType: this.lastDocumentType,
        documentFetchError: this.documentFetchError,

        lastError: this.lastError,
        isLastActionSuccess: this.isLastActionSuccess,

        images: this.images,
        canLoadMoreImages: this.canLoadMoreImages,
        currentImage: this.currentImage,
        lastImageTempUrl: this.lastImageTempUrl,
        currentImageIndex: this.currentImageIndex,
        currentImageTagIndex: this.currentImageTagIndex,
        currentImageTag: this.currentImageTag,

        files: this.files,
        lastFileContent: this.lastFileContent,
        canLoadMoreFiles: this.canLoadMoreFiles,
        currentFile: this.currentFile,
        lastFileTempUrl: this.lastFileTempUrl,
        currentFileIndex: this.currentFileIndex,
        currentFileTagIndex: this.currentFileTagIndex,
        currentFileTag: this.currentFileTag,

        curRewardIndex: this.curRewardIndex,
        curReward: this.curReward,
        curPlayerReward: this.curPlayerReward,

        curTriggerIndex: this.curTriggerIndex,
        curTriggerInfo: this.curTriggerInfo,

        curBonusIndex: this.curBonusIndex,
        curBonus: this.curBonus,

        curSchedulerIndex: this.curSchedulerIndex,
        curSchedulerInfo: this.curSchedulerInfo,
        curSchedulerDayInfo: this.curSchedulerDayInfo,
        curEventIndex: this.curEventIndex,
        curEventInfo: this.curEventInfo,

        currentVariableIndex: this.currentVariableIndex,
        currentVariable: this.currentVariable,

        curSegment: this.curSegment
      }
    }

    LoadFromJson(o) {
      this.leaderboard = o.leaderboard
      this.leaderboardInfo = o.leaderboardInfo || {}
      this.leaderboardRecords = o.leaderboardRecords || {}
      this.leaderboardResult = o.leaderboardResult || {
        abovePlayers: [],
        belowPlayers: [],
        topPlayers: []
      }

      this.currentLeaderboardIndex = o.currentLeaderboardIndex
      this.currentLeaderboardPlayer = o.currentLeaderboardPlayer || {}
      this.lastLeaderboardTag = o.lastLeaderboardTag
      this.lastLeaderboardVariant = o.lastLeaderboardVariant
      this.lastLeaderboardPlayerRatingTag = o.lastLeaderboardPlayerRatingTag
      this.leaderboardPlayerPosition = o.leaderboardPlayerPosition || 0

      this.currentPlayerFieldKey = o.currentPlayerFieldKey
      this.currentPlayerFieldType = o.currentPlayerFieldType
      this.currentPlayerFieldName = o.currentPlayerFieldName
      this.currentPlayerFieldValue = o.currentPlayerFieldValue

      this.currentPlayerFieldVariantValue = o.currentPlayerFieldVariantValue
      this.currentPlayerFieldVariantName = o.currentPlayerFieldVariantName
      this.currentPlayerFieldVariantIndex = o.currentPlayerFieldVariantIndex

      this.isLastAdSuccess = o.isLastAdSuccess
      this.isLastShareSuccess = o.isLastShareSuccess
      this.isLastCommunityJoinSuccess = o.isLastCommunityJoinSuccess

      this.isLastAddShortcutSuccess = o.isLastAddShortcutSuccess

      this.isReady = o.isReady
      this.isPlayerReady = o.isPlayerReady

      this.achievements = o.achievements || []
      this.achievementsGroups = o.achievementsGroups || []
      this.playerAchievements = o.playerAchievements || []

      this.currentAchievementIndex = o.currentAchievementIndex || 0
      this.currentAchievement = o.currentAchievement || {}

      this.currentAchievementsGroupIndex = o.currentAchievementsGroupIndex || 0
      this.currentAchievementsGroupId = o.currentAchievementsGroupId || 0
      this.currentAchievementsGroupTag = o.currentAchievementsGroupTag || ''
      this.currentAchievementsGroupName = o.currentAchievementsGroupName || ''
      this.currentAchievementsGroupDescription =
        o.currentAchievementsGroupDescription || ''

      this.currentPlayerAchievementIndex = o.currentPlayerAchievementIndex || 0
      this.currentPlayerAchievementId = o.currentPlayerAchievementId || 0
      this.currentPlayerAchievementUnlockDate =
        o.currentPlayerAchievementUnlockDate || ''

      this.isUnlockAchievementSuccess = o.isUnlockAchievementSuccess || false
      this.unlockAchievementError = o.unlockAchievementError || ''

      this.products = o.products || []
      this.playerPurchases = o.playerPurchases || []

      this.currentProductIndex = o.currentProductIndex || 0
      this.currentProduct = o.currentProduct || {
        id: 0,
        tag: '',
        name: '',
        description: '',
        icon: '',
        iconSmall: '',
        price: 0,
        currency: '',
        currencySymbol: ''
      }

      this.currentProductPurchases = o.currentProductPurchases || 0

      this.currentPurchaseIndex = o.currentPurchaseIndex || 0
      this.currentPurchase = o.currentPurchase || {
        id: 0,
        tag: '',
        createdAt: ''
      }

      this.lastPlayersTag = o.lastPlayersTag
      this.currentPlayersIndex = o.currenPlayersIndex || 0
      this.currentPlayersPlayer = o.currentPlayersPlayer || {
        state: {},
        achievements: [],
        purchases: []
      }

      this.isSubscribeProductSuccess = o.isSubscribeProductSuccess || false
      this.isUnsubscribeProductSuccess = o.isUnsubscribeProductSuccess || false

      this.isPurchaseProductSuccess = o.isPurchaseProductSuccess || false
      this.purchaseProductError = o.purchaseProductError || ''
      this.purchasedProductId = o.purchasedProductId || 0
      this.purchasedProductTag = o.purchasedProductTag || ''

      this.isConsumeProductSuccess = o.isConsumeProductSuccess || false
      this.consumeProductError = o.consumeProductError || ''
      this.consumedProductId = o.consumedProductId || 0
      this.consumedProductTag = o.consumedProductTag || ''

      this.gamesCollection = o.gamesCollection || {
        id: 0,
        tag: '',
        name: '',
        description: '',
        games: []
      }

      this.currentGameIndex = o.currentGameIndex || 0
      this.currentGameId = o.currentGameId || 0
      this.currentGameName = o.currentGameName || ''
      this.currentGameDescription = o.currentGameDescription || ''
      this.currentGameIcon = o.currentGameIcon || ''
      this.currentGameUrl = o.currentGameUrl || ''

      this.gamesCollectionFetchError = o.gamesCollectionFetchError || ''
      this.lastGamesCollectionIdOrTag = o.lastGamesCollectionIdOrTag || ''

      this.document = o.document || {
        type: '',
        content: ''
      }

      this.lastDocumentType = o.lastDocumentType || ''
      this.documentFetchError = o.documentFetchError || ''

      this.lastError = o.lastError || ''
      this.isLastActionSuccess = o.isLastActionSuccess || false

      this.images = o.images || []
      this.canLoadMoreImages = o.canLoadMoreImages || false
      this.currentImageIndex = o.currentImageIndex || 0
      this.currentImageTagIndex = o.currentImageTagIndex || 0
      this.currentImageTag = o.currentImageTag || ''
      this.currentImage = o.currentImage || {
        id: '',
        playerId: 0,
        width: 0,
        height: 0,
        src: '',
        tags: []
      }

      this.files = o.files || []
      this.lastFileContent = o.lastFileContent || ''
      this.canLoadMoreFiles = o.canLoadMoreFiles || false
      this.currentFileIndex = o.currentFileIndex || 0
      this.currentFileTagIndex = o.currentFileTagIndex || 0
      this.currentFileTag = o.currentFileTag || ''
      this.currentFile = o.currentFile || {
        id: '',
        playerId: 0,
        name: '',
        size: 0,
        src: '',
        tags: []
      }

      this.currentVariableIndex = o.currentVariableIndex || 0
      this.currentVariable = o.currentVariable || {
        key: '',
        type: '',
        value: ''
      }

      this.curRewardIndex = o.curRewardIndex || 0
      this.curReward = o.curReward || {}
      this.curPlayerReward = o.curPlayerReward || {}

      this.curTriggerIndex = o.curTriggerIndex || 0
      this.curTriggerInfo = o.curTriggerInfo || { trigger: {} }

      this.curSchedulerIndex = o.curSchedulerIndex || 0
      this.curSchedulerInfo = o.curSchedulerInfo || { scheduler: {} }
      this.curSchedulerDayInfo = o.curSchedulerDayInfo || { scheduler: {} }
      this.curEventIndex = o.curEventIndex || 0
      this.curEventInfo = o.curEventInfo || { event: {} }

      this.curBonusIndex = o.curBonusIndex || 0
      this.curBonus = o.curBonus || {}

      this.curSegment = o.curSegment || ''
    }

    GetDebuggerProperties() {
      if (!this.isPlayerReady) {
        return []
      }
      return [
        {
          title: 'GS - Base',
          properties: [
            {
              name: 'Language',
              value: this.gp.language
            },
            {
              name: 'Avatar Generator',
              value: this.gp.avatarGenerator
            },
            {
              name: 'Platform',
              value: this.gp.platform.type
            },
            {
              name: 'Last Error',
              value: this.lastError
            },
            {
              name: 'Is Last Action Success',
              value: this.isLastActionSuccess
            },
            {
              name: 'Is Mobile',
              value: this.gp.isMobile
            },
            {
              name: 'Is Dev',
              value: this.gp.isDev
            },
            {
              name: 'Is Paused',
              value: this.gp.isPaused
            },
            {
              name: 'Is Gameplay',
              value: this.gp.isGameplay
            },
            {
              name: 'Is Allowed Origin',
              value: this.gp.isAllowedOrigin
            }
          ]
        },
        {
          title: 'GS - Ads',
          properties: [
            {
              name: 'Last Ad Success',
              value: this.isLastAdSuccess
            },
            {
              name: 'Adblock Enabled',
              value: this.gp.ads.isAdblockEnabled
            }
          ]
        },
        {
          title: 'GS - Leaderboards',
          properties: [
            {
              name: 'Player Position',
              value: this.leaderboardPlayerPosition
            }
          ]
        },
        {
          title: 'GS - Player',
          properties: [
            {
              name: 'ID',
              value: this.gp.player.id
            },
            {
              name: 'Logged In By Platform',
              value: this.gp.player.isLoggedInByPlatform
            },
            {
              name: 'Is Stub',
              value: this.gp.player.isStub
            },
            ...this.gp.player.fields.map((f) => ({
              name: this.gp.player.getFieldName(f.key),
              value: this.gp.player.get(f.key),
              onedit: (v) => this.CallAction(this.actions.PlayerSet, f.key, v)
            }))
          ]
        },
        {
          title: 'GS - Achievements - Current',
          properties: [
            {
              name: 'Index',
              value: this.currentAchievementIndex
            },
            {
              name: 'ID',
              value: this.currentAchievement.id
            },
            {
              name: 'Tag',
              value: this.currentAchievement.tag
            },
            {
              name: 'Name',
              value: this.currentAchievement.name
            },
            {
              name: 'Description',
              value: this.currentAchievement.description
            },
            {
              name: 'Rare',
              value: this.currentAchievement.rare
            },
            {
              name: 'Unlocked',
              value: this.currentAchievement.unlocked
            },
            {
              name: 'Icon',
              value: this.currentAchievement.icon
            },
            {
              name: 'Icon Small',
              value: this.currentAchievement.iconSmall
            },
            {
              name: 'Locked Icon',
              value: this.currentAchievement.lockedIcon
            },
            {
              name: 'Locked Icon Small',
              value: this.currentAchievement.lockedIconSmall
            },
            {
              name: 'Progress',
              value: this.currentAchievement.progress
            },
            {
              name: 'Max Progress',
              value: this.currentAchievement.maxProgress
            },
            {
              name: 'Progress Step',
              value: this.currentAchievement.progressStep
            },
            {
              name: 'Locked Visible',
              value: this.currentAchievement.lockedVisible
            },
            {
              name: 'Locked Description Visible',
              value: this.currentAchievement.lockedDescriptionVisible
            }
          ]
        },
        {
          title: 'GS - Achievements Groups Loop',
          properties: [
            {
              name: 'Current Achievements Group Index',
              value: this.currentAchievementsGroupIndex
            },
            {
              name: 'Current Achievements Group ID',
              value: this.currentAchievementsGroupId
            },
            {
              name: 'Current Achievements Group Tag',
              value: this.currentAchievementsGroupTag
            },
            {
              name: 'Current Achievements Group Name',
              value: this.currentAchievementsGroupName
            },
            {
              name: 'Current Achievements Group Description',
              value: this.currentAchievementsGroupDescription
            }
          ]
        },
        {
          title: 'GS - Player Achievements Loop',
          properties: [
            {
              name: 'Current Player Achievement Index',
              value: this.currentPlayerAchievementIndex
            },
            {
              name: 'Current Player Achievement ID',
              value: this.currentPlayerAchievementId
            },
            {
              name: 'Current Player Achievement Unlock Date',
              value: this.currentPlayerAchievementUnlockDate
            }
          ]
        },
        {
          title: 'GS - Products Loop',
          properties: [
            {
              name: 'Current Product Index',
              value: this.currentProductIndex
            },
            {
              name: 'Current Product ID',
              value: this.currentProduct.id
            },
            {
              name: 'Current Product Tag',
              value: this.currentProduct.tag
            },
            {
              name: 'Current Product Name',
              value: this.currentProduct.name
            },
            {
              name: 'Current Product Description',
              value: this.currentProduct.description
            },
            {
              name: 'Current Product Icon',
              value: this.currentProduct.icon
            },
            {
              name: 'Current Product Icon Small',
              value: this.currentProduct.iconSmall
            },
            {
              name: 'Current Product Icon',
              value: this.currentProduct.icon
            },
            {
              name: 'Current Product Price',
              value: this.currentProduct.price
            },
            {
              name: 'Current Product Currency',
              value: this.currentProduct.currency
            },
            {
              name: 'Current Product CurrencySymbol',
              value: this.currentProduct.currencySymbol
            },
            {
              name: 'Current Product Purchases',
              value: this.currentProductPurchases
            }
          ]
        },
        {
          title: 'GS - Purchased Product',
          properties: [
            {
              name: 'Is purchase successful',
              value: this.isPurchaseProductSuccess
            },
            {
              name: 'Purchase error',
              value: this.purchaseProductError
            },
            {
              name: 'Purchased Product ID',
              value: this.purchasedProductId
            },
            {
              name: 'Purchased Product Tag',
              value: this.purchasedProductTag
            }
          ]
        },
        {
          title: 'GS - Consumed Product',
          properties: [
            {
              name: 'Is consume successful',
              value: this.isConsumeProductSuccess
            },
            {
              name: 'Consume error',
              value: this.consumeProductError
            },
            {
              name: 'Consumed Product ID',
              value: this.consumedProductId
            },
            {
              name: 'Consumed Product Tag',
              value: this.consumedProductTag
            }
          ]
        },
        {
          title: 'GS - Last Games Collection',
          properties: [
            {
              name: 'Collection ID',
              value: this.gamesCollection.id
            },
            {
              name: 'Collection Tag',
              value: this.gamesCollection.tag
            },
            {
              name: 'Collection Name',
              value: this.gamesCollection.name
            },
            {
              name: 'Collection Description',
              value: this.gamesCollection.description
            },
            {
              name: 'Fetch Error',
              value: this.gamesCollectionFetchError
            }
          ]
        },
        {
          title: 'GS - Games in Collection',
          properties: [
            {
              name: 'Current Game Index',
              value: this.currentGameIndex
            },
            {
              name: 'Current Game ID',
              value: this.currentGameId
            },
            {
              name: 'Current Game Name',
              value: this.currentGameName
            },
            {
              name: 'Current Game Description',
              value: this.currentGameDescription
            },
            {
              name: 'Current Game Icon',
              value: this.currentGameIcon
            },
            {
              name: 'Current Game Url',
              value: this.currentGameUrl
            }
          ]
        },
        {
          title: 'GS - Documents',
          properties: [
            {
              name: 'Document Type',
              value: this.document.type
            },
            {
              name: 'Document Content',
              value: this.document.content
            },
            {
              name: 'Fetch Error',
              value: this.documentFetchError
            }
          ]
        }
      ]
    }
  }
}

'use strict';
{
    function each(runtime, array, cb) {
        const eventSheetManager = runtime.GetEventSheetManager();
        const currentEvent = runtime.GetCurrentEvent();
        const solModifiers = currentEvent.GetSolModifiers();
        const eventStack = runtime.GetEventStack();
        const oldFrame = eventStack.GetCurrentStackFrame();
        const newFrame = eventStack.Push(currentEvent);

        array.forEach((item, index) => {
            cb(item, index);

            eventSheetManager.PushCopySol(solModifiers);
            currentEvent.Retrigger(oldFrame, newFrame);
            eventSheetManager.PopSol(solModifiers);
        });

        eventStack.Pop();
    }

    function isExists(obj) {
        return obj && !!obj.id;
    }

    C3.Plugins.Eponesh_GameScore.Cnds = {
        OnPlayerChange() {
            return true;
        },

        OnPlayerSyncComplete() {
            return true;
        },

        OnPlayerSyncError() {
            return true;
        },

        OnPlayerLoadComplete() {
            return true;
        },

        OnPlayerLoadError() {
            return true;
        },

        OnPlayerLoginComplete() {
            return true;
        },

        OnPlayerLoginError() {
            return true;
        },

        OnPlayerFetchFieldsComplete() {
            return true;
        },

        OnPlayerFetchFieldsError() {
            return true;
        },

        OnPlayerReady() {
            return true;
        },

        IsPlayerReady() {
            return this.isPlayerReady;
        },

        IsPlayerStub() {
            return this.gp.player.isStub;
        },

        IsPlayerLoggedIn() {
            return this.gp.player.isLoggedIn;
        },

        PlayerHasKey(key) {
            return this.gp.player.has(key);
        },

        PlayerFieldIsEnum(key) {
            return this.gp.player.getField(key).variants.length;
        },

        PlayerCompareScore(comparison, value) {
            return this.mappers.compare[comparison](this.gp.player.score, value);
        },

        PlayerCompare(key, comparison, value) {
            return this.mappers.compare[comparison](this.gp.player.get(key), value);
        },

        PlayerEachField() {
            each(this._runtime, this.gp.player.fields, (field) => {
                this.currentPlayerFieldKey = field.key;
                this.currentPlayerFieldType = field.type;
                this.currentPlayerFieldName = field.name;
                this.currentPlayerFieldValue = this.gp.player.get(field.key);
            });

            return false;
        },

        PlayerEachFieldVariant(key) {
            each(this._runtime, this.gp.player.getField(key).variants, (variant, index) => {
                this.currentPlayerFieldVariantValue = variant.value;
                this.currentPlayerFieldVariantName = variant.name;
                this.currentPlayerFieldVariantIndex = index;
            });

            return false;
        },

        OnLeaderboardOpen() {
            return true;
        },

        OnLeaderboardClose() {
            return true;
        },

        OnLeaderboardFetch(tag) {
            return this.lastLeaderboardTag === tag;
        },

        OnLeaderboardAnyFetch() {
            return true;
        },

        OnLeaderboardFetchError(tag) {
            return this.lastLeaderboardTag === tag;
        },

        OnLeaderboardAnyFetchError() {
            return true;
        },

        OnLeaderboardFetchPlayer(tag) {
            return this.lastLeaderboardPlayerRatingTag === tag;
        },

        OnLeaderboardAnyFetchPlayer() {
            return true;
        },

        OnLeaderboardFetchPlayerError(tag) {
            return this.lastLeaderboardPlayerRatingTag === tag;
        },

        OnLeaderboardAnyFetchPlayerError() {
            return true;
        },

        OnLeaderboardPublishRecord() {
            return true;
        },

        OnLeaderboardPublishRecordError() {
            return true;
        },

        LeaderboardEachPlayer() {
            each(this._runtime, this.leaderboard, (player, index) => {
                this.currentLeaderboardIndex = index;
                this.currentLeaderboardPlayer = player;
            });

            return false;
        },

        LeaderboardEachTopPlayer() {
            each(this._runtime, this.leaderboardResult.topPlayers, (player, index) => {
                this.currentLeaderboardIndex = index;
                this.currentLeaderboardPlayer = player;
            });

            return false;
        },
        LeaderboardEachAbovePlayer() {
            each(this._runtime, this.leaderboardResult.abovePlayers, (player, index) => {
                this.currentLeaderboardIndex = index;
                this.currentLeaderboardPlayer = player;
            });

            return false;
        },
        LeaderboardEachBelowPlayer() {
            each(this._runtime, this.leaderboardResult.belowPlayers, (player, index) => {
                this.currentLeaderboardIndex = index;
                this.currentLeaderboardPlayer = player;
            });

            return false;
        },

        OnAchievementsOpen() {
            return true;
        },

        OnAchievementsClose() {
            return true;
        },

        OnAchievementsFetch() {
            return true;
        },

        OnAchievementsFetchError() {
            return true;
        },

        OnAchievementsUnlock(idOrTag) {
            const id = parseInt(idOrTag, 10) || 0;
            return this.currentAchievement.tag === idOrTag || this.currentAchievement.id === id;
        },

        OnAchievementsAnyUnlock() {
            return true;
        },

        OnAchievementsAnyUnlockError() {
            return true;
        },

        OnAchievementsSetProgress(idOrTag) {
            const id = parseInt(idOrTag, 10) || 0;
            return this.currentAchievement.tag === idOrTag || this.currentAchievement.id === id;
        },

        OnAchievementsAnySetProgress() {
            return true;
        },

        OnAchievementsAnySetProgressError() {
            return true;
        },

        AchievementsPickAchievement(idOrTag) {
            const { achievement, playerAchievement, achievementGroup } = this.gp.achievements.getAchievement(idOrTag);
            if (!achievement) {
                return false;
            }

            this.currentAchievementIndex = 0;
            this.currentAchievement = achievement;
            if (playerAchievement) {
                this.currentAchievement.unlocked = playerAchievement.unlocked;
                this.currentAchievement.progress = playerAchievement.progress;
            }

            if (achievementGroup) {
                this.currentAchievementsGroupIndex = 0;
                this.currentAchievementsGroupId = achievementGroup.id;
                this.currentAchievementsGroupTag = achievementGroup.tag;
                this.currentAchievementsGroupName = achievementGroup.name;
                this.currentAchievementsGroupDescription = achievementGroup.description;
            }

            return true;
        },

        AchievementsEachAchievement() {
            each(this._runtime, this.gp.achievements.list, (achievement, index) => {
                const { playerAchievement } = this.gp.achievements.getAchievement(achievement.id);
                this.currentAchievementIndex = index;
                this.currentAchievement = achievement;
                if (playerAchievement) {
                    this.currentAchievement.unlocked = playerAchievement.unlocked;
                    this.currentAchievement.progress = playerAchievement.progress;
                }
            });

            return false;
        },

        AchievementsEachAchievementInGroup(idOrTag) {
            const id = parseInt(idOrTag, 10) || 0;
            const group = this.gp.achievements.groupsList.find((ag) => ag.tag === idOrTag || ag.id === id);
            const achievements = group
                ? group.achievements.reduce((list, aId) => {
                      const { achievement, playerAchievement } = this.gp.achievements.getAchievement(aId);
                      if (achievement) {
                          list.push({ achievement, playerAchievement });
                      }
                      return list;
                  }, [])
                : [];

            each(this._runtime, achievements, ({ achievement, playerAchievement }, index) => {
                this.currentAchievementIndex = index;
                this.currentAchievement = achievement;
                if (playerAchievement) {
                    this.currentAchievement.unlocked = playerAchievement.unlocked;
                    this.currentAchievement.progress = playerAchievement.progress;
                }
            });

            return false;
        },

        AchievementsEachAchievementsGroup() {
            each(this._runtime, this.gp.achievements.groupsList, (achievementsGroup, index) => {
                this.currentAchievementsGroupIndex = index;
                this.currentAchievementsGroupId = achievementsGroup.id;
                this.currentAchievementsGroupTag = achievementsGroup.tag;
                this.currentAchievementsGroupName = achievementsGroup.name;
                this.currentAchievementsGroupDescription = achievementsGroup.description;
            });

            return false;
        },

        AchievementsEachPlayerAchievements() {
            each(this._runtime, this.gp.achievements.unlockedList, (playerAchievement, index) => {
                this.currentPlayerAchievementIndex = index;
                this.currentPlayerAchievementId = playerAchievement.id;
                this.currentPlayerAchievementUnlockDate = playerAchievement.createdAt;
            });

            return false;
        },

        IsAchievementsCurAchievementUnlocked() {
            return this.currentAchievement.unlocked;
        },

        IsAchievementsCurAchievementLockedVisible() {
            return !!this.currentAchievement.isLockedVisible;
        },

        IsAchievementsCurAchievementLockedDescriptionVisible() {
            return !!this.currentAchievement.isLockedDescriptionVisible;
        },

        IsAchievementsUnlockSuccessful() {
            return this.isUnlockAchievementSuccess;
        },

        AchievementsIsUnlocked(idOrTag) {
            return this.gp.achievements.has(idOrTag);
        },

        // payments
        OnPaymentsFetchProducts() {
            return true;
        },

        OnPaymentsFetchProductsError() {
            return true;
        },

        OnEventConnect() {
            return true;
        },

        OnPaymentsPurchase(idOrTag) {
            const id = parseInt(idOrTag, 10) || 0;
            return this.purchasedProductTag === idOrTag || this.purchasedProductId === id;
        },

        OnPaymentsPurchaseError(idOrTag) {
            const id = parseInt(idOrTag, 10) || 0;
            return this.purchasedProductTag === idOrTag || this.purchasedProductId === id;
        },

        OnPaymentsAnyPurchase() {
            return true;
        },

        OnPaymentsAnyPurchaseError() {
            return true;
        },

        OnPaymentsConsume(idOrTag) {
            const id = parseInt(idOrTag, 10) || 0;
            return this.consumedProductTag === idOrTag || this.consumedProductId === id;
        },

        OnPaymentsConsumeError(idOrTag) {
            const id = parseInt(idOrTag, 10) || 0;
            return this.consumedProductTag === idOrTag || this.consumedProductId === id;
        },

        OnPaymentsAnyConsume() {
            return true;
        },

        OnPaymentsAnyConsumeError() {
            return true;
        },

        OnPaymentsSubscribe(idOrTag) {
            const id = parseInt(idOrTag, 10) || 0;
            return this.purchasedProductTag === idOrTag || this.purchasedProductId === id;
        },

        OnPaymentsSubscribeError(idOrTag) {
            const id = parseInt(idOrTag, 10) || 0;
            return this.purchasedProductTag === idOrTag || this.purchasedProductId === id;
        },

        OnPaymentsAnySubscribe() {
            return true;
        },

        OnPaymentsAnySubscribeError() {
            return true;
        },

        OnPaymentsUnsubscribe(idOrTag) {
            const id = parseInt(idOrTag, 10) || 0;
            return this.purchasedProductTag === idOrTag || this.purchasedProductId === id;
        },

        OnPaymentsUnsubscribeError(idOrTag) {
            const id = parseInt(idOrTag, 10) || 0;
            return this.purchasedProductTag === idOrTag || this.purchasedProductId === id;
        },

        OnPaymentsAnyUnsubscribe() {
            return true;
        },

        OnPaymentsAnyUnsubscribeError() {
            return true;
        },

        PaymentsPickProduct(idOrTag) {
            const product = this.gp.payments.getProduct(idOrTag);
            if (!product) {
                return false;
            }

            this.currentProductIndex = 0;
            this.currentProduct = product;
            this.currentProductPurchases = this.gp.payments.purchases.filter((a) => a.productId === product.id).length;

            return true;
        },

        PaymentsEachProduct() {
            each(this._runtime, this.gp.payments.products, (product, index) => {
                this.currentProductIndex = index;
                this.currentProduct = product;
                this.currentProductPurchases = this.gp.payments.purchases.filter(
                    (a) => a.productId === product.id
                ).length;
            });

            return false;
        },

        PaymentsEachPurchase() {
            each(this._runtime, this.gp.payments.purchases, (purchase, index) => {
                this.currentPurchaseIndex = index;
                this.currentPurchase = purchase;
            });

            return false;
        },

        IsPaymentsCurProductPurchased() {
            return this.currentProductPurchases > 0;
        },

        IsPaymentsPurchaseSuccessful() {
            return this.isPurchaseProductSuccess;
        },

        IsPaymentsConsumeSuccessful() {
            return this.isConsumeProductSuccess;
        },

        IsPaymentsSubscribeSuccessful() {
            return this.isSubscribeProductSuccess;
        },

        IsPaymentsUnsubscribeSuccessful() {
            return this.isUnsubscribeProductSuccess;
        },

        PaymentsIsPurchased(idOrTag) {
            return this.gp.payments.has(idOrTag);
        },

        PaymentsIsSubscribed(idOrTag) {
            const purchase = this.gp.payments.getPurchase(idOrTag);
            return purchase ? purchase.subscribed || false : false;
        },

        IsPaymentsAvailable() {
            return this.gp.payments.isAvailable;
        },

        IsSubscriptionsAvailable() {
            return this.gp.payments.isSubscriptionsAvailable;
        },

        // images
        OnImagesFetch() {
            return true;
        },

        OnImagesFetchError() {
            return true;
        },

        OnImagesFetchMore() {
            return true;
        },

        OnImagesFetchMoreError() {
            return true;
        },

        OnImagesUpload() {
            return true;
        },

        OnImagesUploadError() {
            return true;
        },

        OnImagesChoose() {
            return true;
        },

        OnImagesChooseError() {
            return true;
        },

        ImagesEachImage() {
            each(this._runtime, this.images, (image, index) => {
                this.currentImageIndex = index;
                this.currentImage = image;
            });

            return false;
        },

        ImagesEachTag(imageId) {
            const image = this.images.find((i) => i.id === imageId);
            const tags = image ? image.tags : [];

            each(this._runtime, tags || [], (tag, index) => {
                this.currentImageTagIndex = index;
                this.currentImageTag = tag;
            });

            return false;
        },

        ImagesCanLoadMore() {
            return this.canLoadMoreImages;
        },

        ImagesCanUpload() {
            return this.gp.images.canUpload;
        },

        // files
        OnFilesFetch() {
            return true;
        },

        OnFilesFetchError() {
            return true;
        },

        OnFilesFetchMore() {
            return true;
        },

        OnFilesFetchMoreError() {
            return true;
        },

        OnFilesUpload() {
            return true;
        },

        OnFilesUploadError() {
            return true;
        },

        OnFilesLoadContent() {
            return true;
        },

        OnFilesLoadContentError() {
            return true;
        },

        OnFilesChoose() {
            return true;
        },

        OnFilesChooseError() {
            return true;
        },

        FilesEachFile() {
            each(this._runtime, this.files, (file, index) => {
                this.currentFileIndex = index;
                this.currentFile = file;
            });

            return false;
        },

        FilesEachTag(fileId) {
            const file = this.files.find((i) => i.id === fileId);
            const tags = file ? file.tags : [];

            each(this._runtime, tags || [], (tag, index) => {
                this.currentFileTagIndex = index;
                this.currentFileTag = tag;
            });

            return false;
        },

        FilesCanLoadMore() {
            return this.canLoadMoreFiles;
        },

        FilesCanUpload() {
            return this.gp.files.canUpload;
        },

        // game variables
        OnVariablesFetch() {
            return true;
        },

        OnVariablesFetchError() {
            return true;
        },

        VariablesEachVariable() {
            each(this._runtime, this.gp.variables.list, (variable, index) => {
                this.currentVariableIndex = index;
                this.currentVariable = variable;
            });

            return false;
        },

        VariablesCompare(key, comparison, value) {
            return this.mappers.compare[comparison](this.gp.variables.get(key), value);
        },

        VariablesCompareType(key, type) {
            return this.gp.variables.type(key) === this.mappers.variablesTypes[type];
        },

        VariablesHas(key) {
            return this.gp.variables.has(key);
        },

        OnFullscreenOpen() {
            return true;
        },

        OnFullscreenClose() {
            return true;
        },

        OnFullscreenChange() {
            return true;
        },

        IsFullscreenMode() {
            return this.gp.fullscreen.isEnabled;
        },

        OnAdsStart() {
            return true;
        },

        OnAdsClose() {
            return true;
        },

        OnAdsFullscreenStart() {
            return true;
        },

        OnAdsFullscreenClose() {
            return true;
        },

        OnAdsPreloaderStart() {
            return true;
        },

        OnAdsPreloaderClose() {
            return true;
        },

        OnAdsRewardedStart() {
            return true;
        },

        OnAdsRewardedClose() {
            return true;
        },

        OnAdsRewardedReward() {
            return true;
        },

        OnAdsStickyStart() {
            return true;
        },

        OnAdsStickyClose() {
            return true;
        },

        OnAdsStickyRefresh() {
            return true;
        },

        OnAdsStickyRender() {
            return true;
        },

        IsAdsFullscreenAvailable() {
            return this.gp.ads.isFullscreenAvailable;
        },

        IsAdsRewardedAvailable() {
            return this.gp.ads.isRewardedAvailable;
        },

        IsAdsPreloaderAvailable() {
            return this.gp.ads.isPreloaderAvailable;
        },

        IsAdsStickyAvailable() {
            return this.gp.ads.isStickyAvailable;
        },

        IsAdsFullscreenPlaying() {
            return this.gp.ads.isFullscreenPlaying;
        },

        CanShowFullscreenBeforeGamePlay() {
            return this.gp.ads.canShowFullscreenBeforeGamePlay;
        },

        IsAdsRewardedPlaying() {
            return this.gp.ads.isRewardedPlaying;
        },

        IsAdsPreloaderPlaying() {
            return this.gp.ads.isPreloaderPlaying;
        },

        IsAdsStickyPlaying() {
            return this.gp.ads.isStickyPlaying;
        },

        IsAdsAdblockEnabled() {
            return this.gp.ads.isAdblockEnabled;
        },

        IsAdsLastAdSuccess() {
            return Boolean(this.isLastAdSuccess);
        },

        // gp
        OnChangeLanguage() {
            return true;
        },

        OnChangeAvatarGenerator() {
            return true;
        },

        OnChangeOrientation() {
            return true;
        },

        OnOverlayReady() {
            return true;
        },

        IsDev() {
            return this.gp.isDev;
        },

        IsMobile() {
            return this.gp.isMobile;
        },

        DeviceType(type) {
            return this.gp.device.type === this.mappers.deviceTypes[type];
        },

        IsAllowedOrigin() {
            return this.gp.isAllowedOrigin;
        },

        IsPortrait() {
            return this.gp.isPortrait;
        },

        Language(language) {
            return this.gp.language === this.mappers.language[language];
        },

        OnPause() {
            return true;
        },

        OnResume() {
            return true;
        },

        IsPaused() {
            return this.gp.isPaused;
        },

        OnGameplayStart() {
            return true;
        },

        OnGameplayStop() {
            return true;
        },

        IsGameplay() {
            return this.gp.isGameplay;
        },

        // platform
        HasPlatformIntegratedAuth() {
            return this.gp.platform.hasIntegratedAuth;
        },

        PlatformType(type) {
            return this.gp.platform.type === this.mappers.platform[type];
        },

        IsExternalLinksAllowedOnPlatform() {
            return this.gp.platform.isExternalLinksAllowed;
        },

        // socials
        OnSocialsShare() {
            return true;
        },

        OnSocialsPost() {
            return true;
        },

        OnSocialsInvite() {
            return true;
        },

        OnSocialsJoinCommunity() {
            return true;
        },

        IsSocialsLastShareSuccess() {
            return this.isLastShareSuccess;
        },

        IsSocialsLastCommunityJoinSuccess() {
            return this.isLastCommunityJoinSuccess;
        },

        IsSocialsSupportsShare() {
            return this.gp.socials.isSupportsShare;
        },

        IsSocialsSupportsNativeShare() {
            return this.gp.socials.isSupportsNativeShare;
        },

        IsSocialsSupportsNativePosts() {
            return this.gp.socials.isSupportsNativePosts;
        },

        IsSocialsSupportsNativeInvite() {
            return this.gp.socials.isSupportsNativeInvite;
        },

        IsSocialsSupportsNativeCommunityJoin() {
            return this.gp.socials.isSupportsNativeCommunityJoin;
        },

        SocialsCanJoinCommunity() {
            return this.gp.socials.canJoinCommunity;
        },

        // app
        OnAppAddShortcut() {
            return true;
        },

        OnAppReview() {
            return true;
        },

        OnAppReviewError() {
            return true;
        },

        IsAppLastAddShortcutSuccess() {
            return this.isLastAddShortcutSuccess;
        },

        AppCanAddShortcut() {
            return this.gp.app.canAddShortcut;
        },

        AppCanRequestReview() {
            return this.gp.app.canRequestReview;
        },

        AppIsAlreadyReviewed() {
            return this.gp.app.isAlreadyReviewed;
        },

        // games collections
        OnGamesCollectionsOpen() {
            return true;
        },

        OnGamesCollectionsClose() {
            return true;
        },

        OnGamesCollectionsFetchAny() {
            return true;
        },

        OnGamesCollectionsFetchAnyError() {
            return true;
        },

        OnGamesCollectionsFetch(idOrTag) {
            return this.lastGamesCollectionIdOrTag === idOrTag;
        },

        OnGamesCollectionsFetchError(idOrTag) {
            return this.lastGamesCollectionIdOrTag === idOrTag;
        },

        GamesCollectionsEachGame() {
            each(this._runtime, this.gamesCollection.games, (game, index) => {
                this.currentGameIndex = index;
                this.currentGameId = game.id;
                this.currentGameName = game.name;
                this.currentGameDescription = game.description;
                this.currentGameIcon = game.icon;
                this.currentGameUrl = game.url;
            });

            return false;
        },

        IsGamesCollectionsAvailable() {
            return this.gp.gamesCollections.isAvailable;
        },

        // documents
        OnDocumentsOpen() {
            return true;
        },

        OnDocumentsClose() {
            return true;
        },

        OnDocumentsFetchAny() {
            return true;
        },

        OnDocumentsFetchAnyError() {
            return true;
        },

        OnDocumentsFetch(type) {
            return this.lastDocumentType === this.mappers.documentTypes[type];
        },

        OnDocumentsFetchError(type) {
            return this.lastDocumentType === this.mappers.documentTypes[type];
        },

        // players
        OnPlayersFetch(tag) {
            return this.lastPlayersTag === tag;
        },

        OnPlayersAnyFetch() {
            return true;
        },

        OnPlayersFetchError(tag) {
            return this.lastPlayersTag === tag;
        },

        OnPlayersAnyFetchError() {
            return true;
        },

        PlayersEachPlayer() {
            each(this._runtime, this.playersList, (player, index) => {
                this.currentPlayersIndex = index;
                this.currentPlayersPlayer = player;
            });

            return false;
        },

        PlayersEachPlayerAchievement() {
            each(this._runtime, this.currentPlayersPlayer.achievements, (playerAchievement, index) => {
                this.currentPlayerAchievementIndex = index;
                this.currentPlayerAchievementId = playerAchievement.id;
                this.currentPlayerAchievementUnlockDate = playerAchievement.createdAt;
            });

            return false;
        },

        PlayersEachPlayerPurchase() {
            each(this._runtime, this.currentPlayersPlayer.purchases, (purchase, index) => {
                this.currentPurchaseIndex = index;
                this.currentPurchase = purchase;
            });

            return false;
        },

        OnRewardsAccept(idOrTag) {
            return !idOrTag || this.isPickedIdOrTag(idOrTag);
        },
        OnRewardsAcceptError(idOrTag) {
            return !idOrTag || this.isPickedIdOrTag(idOrTag);
        },
        OnRewardsGive(idOrTag) {
            return !idOrTag || this.isPickedIdOrTag(idOrTag);
        },
        OnRewardsGiveError(idOrTag) {
            return !idOrTag || this.isPickedIdOrTag(idOrTag);
        },
        RewardsPick(idOrTag) {
            const result = this.gp.rewards.getReward(idOrTag) || {};
            this.setReward(result);
            return isExists(result.reward);
        },
        RewardsEachReward() {
            each(this._runtime, this.gp.rewards.list, (reward, index) => {
                const result = this.gp.rewards.getReward(reward.id);
                this.setReward(result, index);
            });

            return false;
        },
        IsRewardsCurAccepted() {
            return this.curPlayerReward.countAccepted > 0;
        },
        IsRewardsCurGiven() {
            return this.curPlayerReward.countTotal > 0;
        },
        IsRewardsCurHasAutoAccept() {
            return this.curReward.isAutoAccept;
        },
        RewardsHas(idOrTag) {
            return this.gp.rewards.has(idOrTag);
        },
        RewardsHasAccepted(idOrTag) {
            return this.gp.rewards.hasAccepted(idOrTag);
        },
        RewardsHasUnaccepted(idOrTag) {
            return this.gp.rewards.hasUnaccepted(idOrTag);
        },

        OnTriggersActivate(idOrTag) {
            return !idOrTag || this.lastIdOrTag.id === idOrTag || this.lastIdOrTag.tag === idOrTag;
        },
        OnTriggersClaim(idOrTag) {
            return !idOrTag || this.lastIdOrTag.id === idOrTag || this.lastIdOrTag.tag === idOrTag;
        },
        OnTriggersClaimError(idOrTag) {
            return !idOrTag || this.lastIdOrTag.id === idOrTag || this.lastIdOrTag.tag === idOrTag;
        },
        TriggersPick(idOrTag) {
            this.setTriggerInfo(idOrTag);
            return isExists(this.curTriggerInfo.trigger);
        },
        TriggersEachTrigger() {
            each(this._runtime, this.gp.triggers.list, (trigger, index) => {
                this.setTriggerInfo(trigger.id, index);
            });

            return false;
        },
        TriggersEachBonus() {
            each(this._runtime, this.curTriggerInfo.trigger.bonuses || [], (bonus, index) => {
                this.setBonus(bonus, index);
            });

            return false;
        },
        IsTriggersCurActivated() {
            return !!this.curTriggerInfo.isActivated;
        },
        IsTriggersCurClaimed() {
            return !!this.curTriggerInfo.isClaimed;
        },
        IsTriggersCurHasAutoClaim() {
            return !!this.curTriggerInfo.trigger.isAutoClaim;
        },
        TriggersIsActivated(idOrTag) {
            return this.gp.triggers.isActivated(idOrTag);
        },
        TriggersIsClaimed(idOrTag) {
            return this.gp.triggers.isClaimed(idOrTag);
        },

        BonusType(bonusType) {
            return this.curBonus.type === this.mappers.bonusType[bonusType];
        },

        OnSchedulersRegister(idOrTag) {
            return !idOrTag || this.isPickedIdOrTag(idOrTag);
        },
        OnSchedulersRegisterError(idOrTag) {
            return !idOrTag || this.isPickedIdOrTag(idOrTag);
        },
        OnSchedulersClaimDay(idOrTag, day) {
            return this.isPickedSchedulerDayAndTrigger(idOrTag, day);
        },
        OnSchedulersClaimDayError(idOrTag, day) {
            return this.isPickedSchedulerDayAndTrigger(idOrTag, day);
        },
        OnSchedulersClaimDayAdditional(idOrTag, day, triggerIdOrTag) {
            return this.isPickedSchedulerDayAndTrigger(idOrTag, day, triggerIdOrTag);
        },
        OnSchedulersClaimDayAdditionalError(idOrTag, day, triggerIdOrTag) {
            return this.isPickedSchedulerDayAndTrigger(idOrTag, day, triggerIdOrTag);
        },
        OnSchedulersClaimAllDay(idOrTag, day) {
            return this.isPickedSchedulerDayAndTrigger(idOrTag, day);
        },
        OnSchedulersClaimAllDayError(idOrTag, day) {
            return this.isPickedSchedulerDayAndTrigger(idOrTag, day);
        },
        OnSchedulersClaimAllDays(idOrTag) {
            return this.isPickedSchedulerDayAndTrigger(idOrTag);
        },
        OnSchedulersClaimAllDaysError(idOrTag) {
            return this.isPickedSchedulerDayAndTrigger(idOrTag);
        },
        SchedulersPick(idOrTag) {
            this.setSchedulerInfo(idOrTag);
            return isExists(this.curSchedulerInfo.scheduler);
        },
        SchedulersPickDay(idOrTag, day) {
            this.setSchedulerDayInfo(idOrTag, day);
            return isExists(this.curSchedulerDayInfo.scheduler) && this.curSchedulerDayInfo.day === day;
        },
        SchedulersPickDayAdditional(idOrTag, day, triggerIndex) {
            this.setSchedulerDayInfo(idOrTag, day);
            const triggers = this.curSchedulerDayInfo.triggers || [];
            if (!triggers[triggerIndex]) {
                return false;
            }
            this.setTriggerInfo(triggers[triggerIndex].id);
            return !!this.curTriggerInfo.trigger;
        },
        SchedulersEachScheduler() {
            each(this._runtime, this.gp.schedulers.list, (scheduler, index) => {
                this.setSchedulerInfo(scheduler.id, index);
            });

            return false;
        },
        SchedulersEachCurSchedulerDay() {
            const days = Array.from({ length: this.curSchedulerInfo.scheduler.days || 0 });
            each(this._runtime, days, (_, index) => {
                this.setSchedulerDayInfo(this.curSchedulerInfo.scheduler.id, index + 1);
            });

            return false;
        },
        SchedulersEachCurSchedulerDaysClaimed() {
            each(this._runtime, this.curSchedulerInfo.daysClaimed, (day) => {
                this.setSchedulerDayInfo(this.curSchedulerInfo.scheduler.id, day);
            });

            return false;
        },
        SchedulersEachCurSchedulerDayBonuses() {
            each(this._runtime, this.curSchedulerDayInfo.bonuses || [], (bonus, index) => {
                this.setBonus(bonus, index);
            });

            return false;
        },
        SchedulersEachCurSchedulerDayTriggers() {
            each(this._runtime, this.curSchedulerDayInfo.triggers || [], (trigger, index) => {
                this.setTriggerInfo(trigger.id, index);
            });

            return false;
        },
        IsSchedulersCurRegistered() {
            return this.curSchedulerInfo.isRegistered;
        },
        IsSchedulersCurAutoRegister() {
            return !!this.curSchedulerInfo.scheduler.isAutoRegister;
        },
        IsSchedulersCurRepeatable() {
            return this.curSchedulerInfo.scheduler.isRepeat;
        },
        IsSchedulersCurDayReached() {
            return this.curSchedulerDayInfo.isDayReached;
        },
        IsSchedulersCurDayComplete() {
            return this.curSchedulerDayInfo.isDayComplete;
        },
        IsSchedulersCurDayClaimed() {
            return this.curSchedulerDayInfo.isDayClaimed;
        },
        IsSchedulersCurDayAllClaimed() {
            return this.curSchedulerDayInfo.isAllDayClaimed;
        },
        IsSchedulersCurDayCanClaim() {
            return this.curSchedulerDayInfo.canClaimDay;
        },
        IsSchedulersCurDayCanClaimAll() {
            return this.curSchedulerDayInfo.canClaimAllDay;
        },
        SchedulersIsRegistered(idOrTag) {
            return this.gp.schedulers.isRegistered(idOrTag);
        },
        SchedulersIsTodayRewardClaimed(idOrTag) {
            return this.gp.schedulers.isTodayRewardClaimed(idOrTag);
        },
        SchedulersCanClaimDay(idOrTag, day) {
            return this.gp.schedulers.canClaimDay(idOrTag, day);
        },
        SchedulersCanClaimDayAdditional(idOrTag, day, triggerIdOrTag) {
            return this.gp.schedulers.canClaimDayAdditional(idOrTag, day, triggerIdOrTag);
        },
        SchedulersCanClaimAllDay(idOrTag, day) {
            return this.gp.schedulers.canClaimAllDay(idOrTag, day);
        },
        SchedulersCurType(schedulerType) {
            return this.curSchedulerInfo.scheduler.type === this.mappers.schedulerType[schedulerType];
        },

        OnEventsJoin(idOrTag) {
            return !idOrTag || this.isPickedIdOrTag(idOrTag);
        },
        OnEventsJoinError(idOrTag) {
            return !idOrTag || this.isPickedIdOrTag(idOrTag);
        },
        EventsPick(idOrTag) {
            this.setEventInfo(idOrTag);
            return isExists(this.curEventInfo.event);
        },
        EventsEachEvent() {
            each(this._runtime, this.gp.events.list, (event, index) => {
                this.setEventInfo(event.id, index);
            });

            return false;
        },
        EventsEachCurEventTriggers() {
            each(this._runtime, this.curEventInfo.event.triggers || [], (trigger, index) => {
                this.setTriggerInfo(trigger.id, index);
            });

            return false;
        },
        IsEventsCurJoined() {
            return this.curEventInfo.isJoined;
        },
        IsEventsCurActive() {
            return this.curEventInfo.event.isActive;
        },
        IsEventsCurAutoJoin() {
            return this.curEventInfo.event.isAutoJoin;
        },
        EventsHas(idOrTag) {
            return this.gp.events.has(idOrTag);
        },
        EventsIsJoined(idOrTag) {
            return this.gp.events.isJoined(idOrTag);
        },

        ExperimentsHas(tag, cohort) {
            return this.gp.experiments.has(tag, cohort);
        },

        OnSegmentsEnter() {
            return true;
        },
        OnSegmentsLeave() {
            return true;
        },
        SegmentsHas(tag) {
            return this.gp.segments.has(tag);
        },
        SegmentsEachSegment() {
            each(this._runtime, this.gp.segments.list || [], (segment) => {
                this.curSegment = segment;
            });

            return false;
        },

        IsLastActionSuccess() {
            return this.isLastActionSuccess;
        },

        OnLoadJsonError() {
            return true;
        }
    };
}

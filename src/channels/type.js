'use strict';
{
    const PLUGIN_CLASS = SDK.Plugins.GamePush_Channels;

    PLUGIN_CLASS.Type = class GamePushChannelsType extends SDK.ITypeBase {
        constructor(sdkPlugin, iObjectType) {
            super(sdkPlugin, iObjectType);
        }
    };
}

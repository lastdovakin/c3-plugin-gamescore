'use strict';
{
    function each(runtime, array, cb) {
        const eventSheetManager = runtime.GetEventSheetManager();
        const currentEvent = runtime.GetCurrentEvent();
        const solModifiers = currentEvent.GetSolModifiers();
        const eventStack = runtime.GetEventStack();
        const oldFrame = eventStack.GetCurrentStackFrame();
        const newFrame = eventStack.Push(currentEvent);

        array.forEach((item, index) => {
            cb(item, index);

            eventSheetManager.PushCopySol(solModifiers);
            currentEvent.Retrigger(oldFrame, newFrame);
            eventSheetManager.PopSol(solModifiers);
        });

        eventStack.Pop();
    }

    C3.Plugins.GamePush_Channels.Cnds = {
        IsLastActionSuccess() {
            return this.isLastActionSuccess;
        },
        OnLoadJsonError() {
            return true;
        },
        CanLoadMore() {
            return this.canLoadMore;
        },

        EachChannel() {
            each(this._runtime, this.channels.list, (chanel, index) => {
                this.curChannelIndex = index;
                this.curChannel = chanel;
            });

            return false;
        },

        EachCurChannelTag() {
            each(this._runtime, this.curChannel.tags, (tag, index) => {
                this.curTagIndex = index;
                this.curTag = tag;
            });

            return false;
        },
        EachCurChannelMessageTag() {
            each(this._runtime, this.curChannel.messageTags, (tag, index) => {
                this.curTagIndex = index;
                this.curTag = tag;
            });

            return false;
        },

        CurChannelIsPrivate() {
            return !!this.curChannel.private;
        },
        CurChannelIsVisible() {
            return !!this.curChannel.visible;
        },
        CurChannelHasPassword() {
            return !!this.curChannel.hasPassword;
        },
        CurChannelIsJoined() {
            return !!this.curChannel.isJoined;
        },
        CurChannelIsRequestSent() {
            return !!this.curChannel.isRequestSent;
        },
        CurChannelIsInvited() {
            return !!this.curChannel.isInvited;
        },
        CurChannelIsMuted() {
            return !!this.curChannel.isMuted;
        },
        CurChannelIsOwner() {
            return this.curChannel.ownerId === this.gp.player.id;
        },
        CurChannelCheckACL(role, action) {
            const curRole = this.mappers.channelAclRoleWithMyPlayer[role];
            const curAction = this.mappers.channelAclAction[action];
            const acl = getChannelAcl(this.gp, this.curChannel, curRole) || {};
            return !!acl[curAction];
        },

        OnChannelFetch() {
            return true;
        },
        OnChannelFetchError() {
            return true;
        },

        OnChannelsAnyFetch() {
            return true;
        },
        OnChannelsAnyFetchError() {
            return true;
        },

        OnChannelsFetch(label) {
            return this.lastLabel === label;
        },
        OnChannelsFetchError(label) {
            return this.lastLabel === label;
        },

        OnChannelsAnyFetchMore() {
            return true;
        },
        OnChannelsAnyFetchMoreError() {
            return true;
        },

        OnChannelsFetchMore(label) {
            return this.lastLabel === label;
        },
        OnChannelsFetchMoreError(label) {
            return this.lastLabel === label;
        },

        OnChannelsCreate() {
            return true;
        },
        OnChannelsCreateError() {
            return true;
        },

        OnChannelsUpdate() {
            return true;
        },
        OnChannelsUpdateError() {
            return true;
        },

        OnChannelsDelete() {
            return true;
        },
        OnChannelsDeleteError() {
            return true;
        },

        EachMember() {
            each(this._runtime, this.members.list, (member, index) => {
                this.curMemberIndex = index;
                this.curMember = member;
                this.curPlayerIndex = index;
                this.curPlayer = member.state;
            });

            return false;
        },
        CurMemberIsMuted() {
            return (this.curMember.mute && this.curMember.mute.isMuted) || false;
        },
        CurMemberIsOnline() {
            return !!this.curMember.isOnline;
        },
        OnMembersAnyFetch() {
            return true;
        },
        OnMembersAnyFetchError() {
            return true;
        },
        OnMembersFetch(label) {
            return this.lastLabel === label;
        },
        OnMembersFetchError(label) {
            return this.lastLabel === label;
        },
        OnMembersAnyFetchMore() {
            return true;
        },
        OnMembersAnyFetchMoreError() {
            return true;
        },
        OnMembersFetchMore(label) {
            return this.lastLabel === label;
        },
        OnMembersFetchMoreError(label) {
            return this.lastLabel === label;
        },
        OnKick() {
            return true;
        },
        OnKickError() {
            return true;
        },
        OnMute() {
            return true;
        },
        OnMuteError() {
            return true;
        },
        OnUnmute() {
            return true;
        },
        OnUnmuteError() {
            return true;
        },
        OnJoin() {
            return true;
        },
        OnJoinError() {
            return true;
        },
        OnCancelJoin() {
            return true;
        },
        OnCancelJoinError() {
            return true;
        },
        OnLeave() {
            return true;
        },
        OnLeaveError() {
            return true;
        },
        CurPlayerHas(key) {
            return !!this.curPlayer[key];
        },
        OnJoinRequestsFetch() {
            return true;
        },
        OnJoinRequestsFetchError() {
            return true;
        },
        OnJoinRequestsFetchMore() {
            return true;
        },
        OnJoinRequestsFetchMoreError() {
            return true;
        },
        OnSentJoinRequestsFetch() {
            return true;
        },
        OnSentJoinRequestsFetchError() {
            return true;
        },
        OnSentJoinRequestsFetchMore() {
            return true;
        },
        OnSentJoinRequestsFetchMoreError() {
            return true;
        },
        OnAcceptJoinRequest() {
            return true;
        },
        OnAcceptJoinRequestError() {
            return true;
        },
        OnRejectJoinRequest() {
            return true;
        },
        OnRejectJoinRequestError() {
            return true;
        },
        EachJoinRequest() {
            each(this._runtime, this.joinRequests.list, (joinRequest, index) => {
                this.curJoinRequestIndex = index;
                this.curJoinRequest = joinRequest;

                if (this.joinRequests.lastRequest === 'FetchSentJoinRequests') {
                    this.curChannelIndex = 0;
                    this.curChannel = joinRequest.channel || {};
                } else {
                    this.curPlayerIndex = 0;
                    this.curPlayer = joinRequest.player || {};
                }
            });

            return false;
        },
        OnInvitesFetch() {
            return true;
        },
        OnInvitesFetchError() {
            return true;
        },
        OnInvitesFetchMore() {
            return true;
        },
        OnInvitesFetchMoreError() {
            return true;
        },
        OnChannelInvitesFetch() {
            return true;
        },
        OnChannelInvitesFetchError() {
            return true;
        },
        OnChannelInvitesFetchMore() {
            return true;
        },
        OnChannelInvitesFetchMoreError() {
            return true;
        },
        OnSentInvitesFetch() {
            return true;
        },
        OnSentInvitesFetchError() {
            return true;
        },
        OnSentInvitesFetchMore() {
            return true;
        },
        OnSentInvitesFetchMoreError() {
            return true;
        },
        OnSendInvite() {
            return true;
        },
        OnSendInviteError() {
            return true;
        },
        OnCancelInvite() {
            return true;
        },
        OnCancelInviteError() {
            return true;
        },
        OnAcceptInvite() {
            return true;
        },
        OnAcceptInviteError() {
            return true;
        },
        OnRejectInvite() {
            return true;
        },
        OnRejectInviteError() {
            return true;
        },
        EachInvite() {
            each(this._runtime, this.invites.list, (invite, index) => {
                this.curInviteIndex = index;
                this.curInvite = invite;

                if (this.invites.lastRequest === 'FetchSentInvites') {
                    this.curChannelIndex = 0;
                    this.curChannel = invite.channel || {};
                    this.curPlayerIndex = 0;
                    this.curPlayer = invite.playerTo || {};
                } else if (this.invites.lastRequest === 'FetchChannelInvites') {
                    this.curPlayerIndex = 0;
                    this.curPlayer = invite.playerTo || {};
                } else {
                    this.curChannelIndex = 0;
                    this.curChannel = invite.channel || {};
                    this.curPlayerIndex = 0;
                    this.curPlayer = invite.playerFrom || {};
                }
            });

            return false;
        },
        CurInvitePickPlayerTo() {
            this.curPlayer = (this.curInvite ? this.curInvite.playerTo : {}) || {};
            return true;
        },
        CurInvitePickPlayerFrom() {
            this.curPlayer = (this.curInvite ? this.curInvite.playerFrom : {}) || {};
            return true;
        },

        OnMessagesAnyFetch() {
            return true;
        },
        OnMessagesAnyFetchError() {
            return true;
        },

        OnMessagesFetch(label) {
            return this.lastLabel === label;
        },
        OnMessagesFetchError(label) {
            return this.lastLabel === label;
        },

        OnMessagesAnyFetchMore() {
            return true;
        },
        OnMessagesAnyFetchMoreError() {
            return true;
        },

        OnMessagesFetchMore(label) {
            return this.lastLabel === label;
        },
        OnMessagesFetchMoreError(label) {
            return this.lastLabel === label;
        },

        OnPersonalMessagesAnyFetch() {
            return true;
        },
        OnPersonalMessagesAnyFetchError() {
            return true;
        },

        OnPersonalMessagesFetch(label) {
            return this.lastLabel === label;
        },
        OnPersonalMessagesFetchError(label) {
            return this.lastLabel === label;
        },

        OnPersonalMessagesAnyFetchMore() {
            return true;
        },
        OnPersonalMessagesAnyFetchMoreError() {
            return true;
        },

        OnPersonalMessagesFetchMore(label) {
            return this.lastLabel === label;
        },
        OnPersonalMessagesFetchMoreError(label) {
            return this.lastLabel === label;
        },

        OnFeedMessagesAnyFetch() {
            return true;
        },
        OnFeedMessagesAnyFetchError() {
            return true;
        },

        OnFeedMessagesFetch(label) {
            return this.lastLabel === label;
        },
        OnFeedMessagesFetchError(label) {
            return this.lastLabel === label;
        },

        OnFeedMessagesAnyFetchMore() {
            return true;
        },
        OnFeedMessagesAnyFetchMoreError() {
            return true;
        },

        OnFeedMessagesFetchMore(label) {
            return this.lastLabel === label;
        },
        OnFeedMessagesFetchMoreError(label) {
            return this.lastLabel === label;
        },

        EachMessage() {
            each(this._runtime, this.messages.list, (message, index) => {
                this.curMessageIndex = index;
                this.curMessage = message;
                this.curPlayerIndex = 0;
                this.curPlayer = message.player || {};
            });

            return false;
        },

        EachMessageReversed() {
            const reversedMessages = [].concat(this.messages.list).reverse();
            each(this._runtime, reversedMessages, (message, index) => {
                this.curMessageIndex = index;
                this.curMessage = message;
                this.curPlayerIndex = 0;
                this.curPlayer = message.player || {};
            });

            return false;
        },

        EachCurMessageTag() {
            each(this._runtime, this.curMessage.tags, (tag, index) => {
                this.curTagIndex = index;
                this.curTag = tag;
            });

            return false;
        },

        CurMessageTarget(target) {
            const curTarget = this.mappers.messageTargets[target];
            return this.curMessage.target === curTarget;
        },

        OnMessagesSend() {
            return true;
        },
        OnMessagesSendError() {
            return true;
        },

        OnMessagesEdit() {
            return true;
        },
        OnMessagesEditError() {
            return true;
        },

        OnMessagesDelete() {
            return true;
        },
        OnMessagesDeleteError() {
            return true;
        },

        OnEvent() {
            return true;
        },
        OnEventInvite() {
            return true;
        },
        OnEventCancelInvite() {
            return true;
        },
        OnEventRejectInvite() {
            return true;
        },
        OnEventJoin() {
            return true;
        },
        OnEventJoinRequest() {
            return true;
        },
        OnEventLeave() {
            return true;
        },
        OnEventCancelJoin() {
            return true;
        },
        OnEventRejectJoinRequest() {
            return true;
        },
        OnEventMessage() {
            return true;
        },
        OnEventEditMessage() {
            return true;
        },
        OnEventDeleteMessage() {
            return true;
        },
        OnEventMute() {
            return true;
        },
        OnEventUnmute() {
            return true;
        },
        OnEventUpdateChannel() {
            return true;
        },
        OnEventDeleteChannel() {
            return true;
        },
        EventType(type) {
            const curType = this.mappers.events[type];
            return this.curEvent.type === curType;
        },
        LeaveReason(reason) {
            const curReason = this.mappers.leaveReasons[reason];
            return this.leaveReason === curReason;
        },
        IsMainChatEnabled() {
            return this.gp.channels.isMainChatEnabled || false;
        },
        OnOpenChat() {
            return true;
        },
        OnOpenChatError() {
            return true;
        },
        OnCloseChat() {
            return true;
        },
        OnEventUnmute() {
            return true;
        }
    };

    function getChannelAcl(gp, channel, role) {
        switch (role) {
            case 'myPlayer': {
                if (gp.player.id === channel.ownerId) {
                    return channel.ownerAcl;
                } else if (channel.isJoined) {
                    return channel.memberAcl;
                } else {
                    return channel.guestAcl;
                }
            }
            case 'owner': {
                return channel.ownerAcl;
            }
            case 'member': {
                return channel.memberAcl;
            }
            case 'guest': {
                return channel.guestAcl;
            }
            default: {
                return channel.guestAcl;
            }
        }
    }
}

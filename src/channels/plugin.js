'use strict';
{
    const PLUGIN_ID = 'GamePush_Channels';
    const PLUGIN_VERSION = '1.0.33';
    const PLUGIN_CATEGORY = 'platform-specific';

    const PLUGIN_CLASS = (SDK.Plugins.GamePush_Channels = class GamePushChannelsPlugin extends SDK.IPluginBase {
        constructor() {
            super(PLUGIN_ID);

            SDK.Lang.PushContext('plugins.' + PLUGIN_ID.toLowerCase());

            this._info.SetName(lang('.name'));
            this._info.SetDescription(lang('.description'));
            this._info.SetVersion(PLUGIN_VERSION);
            this._info.SetCategory(PLUGIN_CATEGORY);
            this._info.SetAuthor('GamePush, FZCO');
            this._info.SetHelpUrl(lang('.help-url'));
            this._info.SetIsSingleGlobal(true);

            this._info.SetSupportedRuntimes(['c3']);

            SDK.Lang.PushContext('.properties');

            this._info.SetProperties([
                new SDK.PluginProperty('check', 'enabled', true),
            ]);

            SDK.Lang.PopContext(); //.properties
            SDK.Lang.PopContext();
        }
    });

    PLUGIN_CLASS.Register(PLUGIN_ID, PLUGIN_CLASS);
}

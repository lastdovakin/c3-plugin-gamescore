const zipdir = require('zip-dir');
const fs = require('fs');

function buildPlugin(name) {
    const rootPath = `./dist/src/${name}`;
    const addon = require(`${rootPath}/addon.json`);
    const packageJson = require('./package.json');

    const versions = packageJson.version.split('.').map(Number);
    // minor
    versions[2] += 1;
    const version = versions.join('.');

    if (name === 'core') {
        packageJson.version = version;
        fs.writeFileSync('./package.json', JSON.stringify(packageJson, null, 4));
    }

    addon.version = version;
    fs.writeFileSync(`${rootPath}/addon.json`, JSON.stringify(addon, null, 4));

    const addonName = name === 'core' ? 'Eponesh_GameScore' : 'GamePush_Channels';

    const pluginPath = `${rootPath}/plugin.js`;
    const plugin = fs.readFileSync(pluginPath).toString();
    const pluginWithVersion = plugin.replace(
        new RegExp(`const a="${addonName}",b="(.+)",c=`),
        `const a="${addonName}",b="${version}",c=`
    );

    fs.writeFileSync(pluginPath, pluginWithVersion);

    const instancePath = `${rootPath}/c3runtime/instance.js`;
    const instance = fs.readFileSync(instancePath).toString();
    const instanceWithProdUrl = instance.replace(`"https://localhost:9000"`, `"https://gs.eponesh.com/sdk"`);
    fs.writeFileSync(instancePath, instanceWithProdUrl);

    const path = `build/gamepush-${name}-${version}.c3addon`;
    const hrstart = process.hrtime();

    zipdir(rootPath, { saveTo: path }, () => {
        fs.writeFileSync(instancePath, instance);

        const [, timeEnd] = process.hrtime(hrstart);
        console.log('Successful build:', path);
        console.info('Execution time: %ds', (timeEnd / 1000000000).toFixed(2));
    });
}

buildPlugin('channels');
buildPlugin('core');

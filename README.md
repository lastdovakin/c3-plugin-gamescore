# Construct 3 [PLUGIN]: GamePush
Cross-publisher SDK for management Ads, Player state and game saves, Leaderboards and Analytics with multi languages.

![Actions](./docs/actions-list.jpg)

[EXAMPLE on Yandex.Games](https://yandex.ru/games/play/99431?draft=true)

[EXAMPLE on VK Games](https://vk.com/app7829767)

[EXAMPLE without platform](https://preview.eponesh.com/gamescore/c3)

## Install

From [build](build/) dist.

## For Developers

### Local development

1. Install dependencies

```sh
npm i
```

2. [Enable developer mode](https://www.construct.net/en/make-games/manuals/addon-sdk/guide/enabling-developer-mode).

3. Run dev server
```sh
npm run dev
```

Server started on
[`http://localhost:8080`](http://localhost:8080).

Use `addon.json` link [`http://localhost:8080/addon.json`](http://localhost:8080/addon.json) to load addon.

4. Read [how to use developer mode](https://www.construct.net/en/make-games/manuals/addon-sdk/guide/using-developer-mode).

### Production build

```sh
npm run build
# out >> build/gamepush-X.X.X.c3addon
```
